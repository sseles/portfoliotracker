﻿using System.Threading;
using System.Windows;
using System.Windows.Threading;
using Skeleton.Interfaces;

namespace Skeleton.Popups
{
    public partial class ProgressBox : Window, IProgressUpdate
    {
        public bool Status { get; private set; }

        private CancellationTokenSource _cancellationTokenSource;

        private const string PROGRESS_PERCENTAGE = "{0}%";

        private readonly double _step;

        protected ProgressBox()
        {
            InitializeComponent();

            Status = true;
        }

        public ProgressBox(string message) : this()
        {
            TextBlock.Text = message;
        }

        public ProgressBox(string message, double min, double max, double step = 1) : this(message)
        {
            ProgressBar.IsIndeterminate = false;

            ProgressBar.Minimum = min;

            ProgressBar.Maximum = max;

            ProgressBar.Value = min;

            _step = step;
        }

        public CancellationToken? Show(bool cancelable = false)
        {
            Button.Visibility = cancelable ? Visibility.Visible : Visibility.Collapsed;

            base.Show();

            if (cancelable == true)
            {
                _cancellationTokenSource = new CancellationTokenSource();

                return _cancellationTokenSource.Token;
            }

            return null;
        }

        public void UpdateProgress()
        {
            if (ProgressBar.IsIndeterminate == true) return;

            Dispatcher.BeginInvoke(() =>
            {
                ProgressBar.Value += _step;

                TextBlockPercentage.Text = string.Format(PROGRESS_PERCENTAGE, ProgressBar.Value);

                if (ProgressBar.Value >= ProgressBar.Maximum)
                {
                    OnFinish();
                }
            }, DispatcherPriority.Render);
        }

        public void Finish()
        {
            Dispatcher.BeginInvoke(OnFinish, DispatcherPriority.Render);
        }

        private void OnFinish()
        {
            if (ProgressBar.IsIndeterminate == true)
            {
                ProgressBar.Value = ProgressBar.Maximum;
            }
            Close();
        }

        private void ButtonCancel_OnClick(object sender, RoutedEventArgs e)
        {
            _cancellationTokenSource?.Cancel();

            Status = false;
        }
    }
}
