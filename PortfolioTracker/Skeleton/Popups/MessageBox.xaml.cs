﻿using System.Windows;

namespace Skeleton.Popups
{
    public partial class MessageBox : Window
    {
        public MessageBoxResult Result { get; private set; }

        public MessageBox()
        {
            InitializeComponent();
        }

        public MessageBoxResult ShowDialog(string title, string message, MessageBoxButton boxButtons = MessageBoxButton.OK)
        {
            GroupBox.Header = title;

            TextBlock.Text = message;

            EnableButtons(boxButtons);

            base.ShowDialog();

            return Result;
        }

        private void ButtonOk_OnClick(object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.OK;

            OnClose();
        }

        private void ButtonYes_OnClick(object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.Yes;

            OnClose();
        }

        private void ButtonNo_OnClick(object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.No;

            OnClose();
        }

        private void ButtonCancel_OnClick(object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.Cancel;

            OnClose();
        }

        private void OnClose()
        {
            Close();
        }

        private void EnableButtons(MessageBoxButton buttons)
        {
            switch (buttons)
            {
                case MessageBoxButton.YesNoCancel:
                    ButtonYes.Visibility = Visibility.Visible;
                    ButtonNo.Visibility = Visibility.Visible;
                    ButtonCancel.Visibility = Visibility.Visible;
                    break;
                case MessageBoxButton.YesNo:
                    ButtonYes.Visibility = Visibility.Visible;
                    ButtonNo.Visibility = Visibility.Visible;
                    break;
                case MessageBoxButton.OKCancel:
                    ButtonOk.Visibility = Visibility.Visible;
                    ButtonCancel.Visibility = Visibility.Visible;
                    break;
                default:
                    ButtonOk.Visibility = Visibility.Visible;
                    break;
            }
        }
    }
}
