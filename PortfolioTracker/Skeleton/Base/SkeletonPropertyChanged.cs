﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Skeleton.Base
{
    public abstract class SkeletonPropertyChanged : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected SkeletonPropertyChanged()
        {
            
        }

        public virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
