﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Skeleton.Interfaces;

namespace Skeleton.Base
{
    public class SkeletonModuleBuilder
    {
        private readonly List<ISkeletonModule> _modules;

        public SkeletonModuleBuilder()
        {
            _modules = new List<ISkeletonModule>();
        }

        public void AddModule<Module>() where Module : ISkeletonModule
        {
            _modules.Add((ISkeletonModule)Activator.CreateInstance(typeof(Module)));
        }

        public void RegisterPages(DataTemplateDictionary dictionary)
        {
            foreach (ISkeletonModule module in _modules)
            {
                module.RegisterPages(dictionary);
            }
        }

        public void ConfigureServices(ServiceCollection services)
        {
            foreach (ISkeletonModule module in _modules)
            {
                module.ConfigureServices(services);
            }
        }
    }
}
