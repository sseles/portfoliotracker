﻿using System;
using System.Collections.Generic;
using System.Windows;
using Skeleton.Interfaces;

namespace Skeleton.Base
{
    public class DataTemplateDictionary
    {
        private readonly ResourceDictionary _dictionary;

        private readonly List<Type> _viewModels;

        public DataTemplateDictionary()
        {
            _dictionary = new ResourceDictionary();

            _viewModels= new List<Type>();
        }

        public void AddPage<Page>() where Page : IPageViewModel
        {
            Type viewType = GetViewType<Page>();

            string viewName = GetViewName<Page>();

            FrameworkElementFactory factory = new FrameworkElementFactory(viewType)
            {
                Name = viewName
            };

            DataTemplate template = new DataTemplate
            {
                DataType = typeof(Page),

                VisualTree = factory
            };

            _dictionary.BeginInit();

            _dictionary.Add(template.DataTemplateKey, template);

            _dictionary.EndInit();

            _viewModels.Add(typeof(Page));
        }

        public ResourceDictionary GetDictionary()
        {
            return _dictionary;
        }

        public List<Type> GetViewModelTypes()
        {
            return _viewModels;
        }

        private static string GetViewName<Page>()
        {
            string viewName = typeof(Page).FullName;

            int dotIndex = viewName.LastIndexOf('.') + 1;

            return viewName.Substring(dotIndex, viewName.Length - dotIndex - 5);
        }

        private static Type GetViewType<Page>()
        {
            string viewModelName = typeof(Page).AssemblyQualifiedName;

            string viewName;

            int folderIndex = viewModelName.IndexOf("Model", StringComparison.Ordinal);

            int fileIndex = viewModelName.LastIndexOf("Model", StringComparison.Ordinal);

            viewName = viewModelName.Substring(0, folderIndex);

            viewName += viewModelName.Substring(folderIndex + 5, fileIndex - folderIndex - 5);

            viewName += viewModelName.Substring(fileIndex + 5);

            return Type.GetType(viewName);
        }
    }
}
