﻿using System;

namespace Skeleton.Communications
{
    public class EventDisposable : IDisposable
    {
        private readonly IDisposable disposable;

        public bool IsDisposed { get; private set; }

        public EventDisposable(IDisposable disposable)
        {
            IsDisposed = false;

            this.disposable = disposable;
        }

        public void Dispose()
        {
            IsDisposed = true;

            disposable.Dispose();

            GC.SuppressFinalize(this);
        }
    }
}
