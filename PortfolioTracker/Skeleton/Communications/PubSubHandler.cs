﻿using System;
using System.Collections.Generic;
using System.Reactive.Subjects;

namespace Skeleton.Communications
{
    /// <summary>
    /// Event system with integrated logging and error handling
    /// </summary>
    /// <typeparam name="T">Declare a type of object used for raising the events</typeparam>
    public class PubSubHandler<T>
    {
        protected readonly ISubject<T> subject;

        protected bool allowSubscribing = true;

        protected bool running = true;

        /// <summary>
        /// List of all subscribed events
        /// </summary>
        public List<EventDisposable> Subscribers { get; set; }

        /// <summary>
        /// Set function to be called on error, System Log is already embedded no need to call twice
        /// </summary>
        public Action OnErrorSetter { get; set; }

        /// <summary>
        /// Set function to be called on completed
        /// </summary>
        public Action OnCompletedSetter { get; set; }

        /// <summary>
        /// Check if the Event Handler is setup to be one event only
        /// </summary>
        public bool IsSingleSubscriber { get; private set; }

        /// <summary>
        /// Constructor with default setup to allow multiple events to be subscribed
        /// </summary>
        /// <param name="singleSubscriber">Set to true if you want only one event to be allowed</param>
        public PubSubHandler(bool singleSubscriber = false)
        {
            subject = new Subject<T>();

            Subscribers = new List<EventDisposable>();

            IsSingleSubscriber = singleSubscriber;
        }

        /// <summary>
        /// Subscribe a function to be called whenever the event is triggered
        /// If the EventHandler is setup to be Single subscriber and more than one event is subscribed this will throw IndexOutOfRangeException
        /// </summary>
        /// <param name="subscriber">Action that will be called</param>
        /// <returns>Disposable object of the subscribed event, this is used for unsubscribing</returns>
        public virtual EventDisposable Subscribe(Action<T> subscriber)
        {
            if (IsSingleSubscriber == false || allowSubscribing == true)
            {
                allowSubscribing = false;

                EventDisposable disposable = new EventDisposable(subject.Subscribe(t =>
                {
                    // Execute action
                    try
                    {
                        subscriber(t);
                    }
                    catch (Exception e)
                    {
                        subject.OnError(e);
                    }
                }, OnError, OnCompleted));

                Subscribers.Add(disposable);

                return disposable;
            }
            else
            {
                subject.OnError(new IndexOutOfRangeException());
            }

            return null;
        }

        /// <summary>
        /// Unsubscribe all events
        /// </summary>
        public void UnSubscribe()
        {
            foreach (EventDisposable subscriber in Subscribers)
            {
                subscriber.Dispose();
            }

            Subscribers.Clear();

            if (IsSingleSubscriber == true)
            {
                allowSubscribing = true;
            }
        }

        /// <summary>
        /// Unsubscribe the event
        /// </summary>
        /// <param name="subscriber">Event that will be unsubscribed</param>
        public void UnSubscribe(EventDisposable subscriber)
        {
            subscriber.Dispose();

            Subscribers.Remove(subscriber);

            if (IsSingleSubscriber == true)
            {
                allowSubscribing = true;
            }
        }

        /// <summary>
        /// Raise event by sending a object of T type
        /// </summary>
        /// <param name="item">Object of the T type</param>
        public virtual void Publish(T item)
        {
            if (running == false) return;
            try
            {
                subject.OnNext(item);
            }
            catch (Exception ex)
            {
                subject.OnError(ex);
            }
        }

        protected void OnError(Exception error)
        {
            OnErrorSetter?.DynamicInvoke(error);
        }

        protected void OnCompleted()
        {
            OnCompletedSetter?.DynamicInvoke();
        }

        /// <summary>
        /// Pause event system without disposing, only block events from being pushed
        /// </summary>
        public void Pause()
        {
            running = false;
        }

        /// <summary>
        /// Resume event system 
        /// </summary>
        public void Resume()
        {
            running = true;
        }

        /// <summary>
        /// Dispose all of the events subscribed, you can't add any more events
        /// </summary>
        public virtual void Stop()
        {
            UnSubscribe();

            subject.OnCompleted();
        }
    }
}