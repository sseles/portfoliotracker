﻿using Skeleton.Base;
using Skeleton.Interfaces;

namespace Skeleton
{
    public abstract class SkeletonViewModel : SkeletonPropertyChanged, IPageViewModel
    {
    }
}
