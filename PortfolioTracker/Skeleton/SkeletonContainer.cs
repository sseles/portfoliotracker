﻿using Microsoft.Extensions.DependencyInjection;
using Skeleton.Interfaces;

namespace Skeleton
{
    public class SkeletonContainer
    {
        private ServiceProvider _serviceProvider;
        
        public void SetServiceProvider(ServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IPageViewModel RequestViewModel<ViewModel>() where ViewModel : IPageViewModel
        {
            return _serviceProvider.GetService<ViewModel>();
        }
    }
}
