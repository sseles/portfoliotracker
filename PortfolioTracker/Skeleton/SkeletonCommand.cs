﻿using System;
using System.Windows.Input;

namespace Skeleton
{
    public class SkeletonCommand : ICommand
    {
        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }

        private readonly Predicate<object> _canExecute;

        private readonly Action<object> _execute;

        public SkeletonCommand(Action<object> execute) : this(execute, null)
        {
        }

        public SkeletonCommand(Action<object> execute, Predicate<object> canExecute)
        {
            if (execute == null) throw new ArgumentNullException("Execute function is null");

            _execute = execute;

            _canExecute = canExecute;
        }

        public bool CanExecute(object parameters)
        {
            if (_canExecute == null)
            {
                return true;
            }
            else
            {
                return _canExecute(parameters);
            }
        }

        public void Execute(object parameters)
        {
            _execute(parameters);
        }
    }
}
