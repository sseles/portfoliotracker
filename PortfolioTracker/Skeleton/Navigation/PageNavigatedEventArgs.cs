﻿using Skeleton.Interfaces;

namespace Skeleton.Navigation
{
    public class PageNavigatedEventArgs
    {
        public IPageNavigation CurrentPage { get; private set; }
        public IPageNavigation PreviousPage { get; private set; }

        public PageNavigatedEventArgs(IPageNavigation currentPage, IPageNavigation previousPage)
        {
            CurrentPage = currentPage;

            PreviousPage = previousPage;
        }
    }
}
