﻿using System;
using System.Collections.Generic;
using Skeleton.Interfaces;

namespace Skeleton.Navigation
{
    public class SkeletonNavigation
    {
        public const string DEFAULT_DOMAIN = "DefaultDomain";

        private readonly Dictionary<string, DomainConductor> _domains;

        private readonly SkeletonContainer _container;

        private IPageNavigation _previousPage;

        private DomainConductor _selectedDomain;

        public SkeletonNavigation(SkeletonContainer container)
        {
            _container = container;

            _domains = new Dictionary<string, DomainConductor>();

            AddDomain(DEFAULT_DOMAIN);

            SelectDomain(DEFAULT_DOMAIN);
        }

        public void Navigate<Page>(object obj = null) where Page : IPageNavigation
        {
            PageNavigatedEventArgs e = new PageNavigatedEventArgs((Page)_container.RequestViewModel<Page>(), _previousPage);

            _previousPage?.NavigateFrom();

            _previousPage = e.CurrentPage;

            _selectedDomain.Publish(e);

            _previousPage.NavigateTo(obj);
        }

        #region Subscribtion
        public void Subscribe(Action<PageNavigatedEventArgs> action)
        {
            _selectedDomain.Subscribe(action);
        }

        public void Subscribe(Action<PageNavigatedEventArgs> action, string domain)
        {
            _domains[domain].Subscribe(action);
        }

        public void UnSubscribe()
        {
            _selectedDomain.UnSubscribe();
        }

        public void UnSubscribe( string domain)
        {
            _domains[domain].UnSubscribe();
        }
        #endregion

        #region Domain
        public void SelectDomain(string name)
        {
            if (_domains.ContainsKey(name) == false) throw new Exception("Domain with the selected name does not exists!");

            _selectedDomain = _domains[name];

            _previousPage = null;
        }

        public void AddDomain(string name)
        {
            if (_domains.ContainsKey(name)) throw new Exception("Domain already registered!");

            _domains.Add(name, new DomainConductor(name));
        }

        public void RemoveDomain(string name)
        {
            if (_domains.ContainsKey(name) == false) throw new Exception("Domain with the selected name does not exists!");
            if (name == DEFAULT_DOMAIN) throw new Exception("Default Domain can not be deleted!");

            _domains.Remove(name);
        }
        #endregion
    }
}
