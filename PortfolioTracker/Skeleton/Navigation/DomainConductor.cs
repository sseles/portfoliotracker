﻿using System;
using Skeleton.Communications;

namespace Skeleton.Navigation
{
    public class DomainConductor
    {
        public string Name { get; private set; }

        private readonly PubSubHandler<PageNavigatedEventArgs> _handler;

        public DomainConductor(string name)
        {
            Name = name;

            _handler = new PubSubHandler<PageNavigatedEventArgs>(true);
        }

        public void Subscribe(Action<PageNavigatedEventArgs> action)
        {
            _handler.Subscribe(action);
        }

        public void UnSubscribe()
        {
            _handler.UnSubscribe();
        }

        public void Publish(PageNavigatedEventArgs e)
        {
            _handler.Publish(e);
        }
    }
}
