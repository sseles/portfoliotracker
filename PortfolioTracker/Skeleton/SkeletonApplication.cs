﻿using System;
using System.Collections.Generic;
using System.Windows;
using Microsoft.Extensions.DependencyInjection;
using Skeleton.Base;
using Skeleton.Navigation;

namespace Skeleton
{
    public abstract class SkeletonApplication : Application
    {
        private ServiceProvider _serviceProvider;

        private readonly DataTemplateDictionary _dataTemplates;

        private readonly SkeletonModuleBuilder _moduleBuilder;

        protected SkeletonApplication()
        {
            _dataTemplates = new DataTemplateDictionary();

            _moduleBuilder = new SkeletonModuleBuilder();
        }

        public void StartUp<ShellWindow>() where ShellWindow : Window
        {
            ServiceCollection serviceCollection = new ServiceCollection();

            RegisterModules(_moduleBuilder);

            serviceCollection.AddSingleton<ShellWindow>();

            serviceCollection.AddSingleton<SkeletonContainer>();

            serviceCollection.AddSingleton<SkeletonNavigation>();

            _moduleBuilder.ConfigureServices(serviceCollection);

            _moduleBuilder.RegisterPages(_dataTemplates);

            RegisterPages(_dataTemplates);

            RegisterViewModels(serviceCollection, _dataTemplates.GetViewModelTypes());

            ConfigureServices(serviceCollection);

            _serviceProvider = serviceCollection.BuildServiceProvider();

            _serviceProvider.GetService<SkeletonContainer>().SetServiceProvider(_serviceProvider);

            Current.Resources.MergedDictionaries.Add(_dataTemplates.GetDictionary());

            OnStartup(_serviceProvider);

            _serviceProvider.GetService<ShellWindow>().Show();
        }

        private static void RegisterViewModels(ServiceCollection serviceCollection, IEnumerable<Type> viewModels)
        {
            foreach (Type viewModel in viewModels)
            {
                serviceCollection.AddTransient(viewModel);
            }
        }

        #region Abstract
        public abstract void RegisterPages(DataTemplateDictionary dictionary);
        public abstract void RegisterModules(SkeletonModuleBuilder builder);
        public abstract void ConfigureServices(ServiceCollection services);
        public abstract void OnStartup(ServiceProvider serviceProvider);
        #endregion
    }
}