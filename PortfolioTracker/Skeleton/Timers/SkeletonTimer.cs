﻿using System;
using System.Threading;

namespace Skeleton.Timers
{
    public class SkeletonTimer : IDisposable
    {
        private readonly Timer _timer;

        public int Interval { get; set; } = 1000;

        public SkeletonTimer(TimerCallback callback)
        {
            _timer = new Timer(callback);
        }

        public void Start(int delay = 0)
        {
            _timer.Change(delay, Interval);
        }

        public void Stop()
        {
            _timer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        public void Dispose()
        {
            _timer?.Dispose();

            GC.SuppressFinalize(this);
        }
    }
}
