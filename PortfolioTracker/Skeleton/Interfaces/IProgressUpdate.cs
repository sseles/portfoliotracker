﻿namespace Skeleton.Interfaces
{
    public interface IProgressUpdate
    {
        public void UpdateProgress();

        public void Finish();
    }
}
