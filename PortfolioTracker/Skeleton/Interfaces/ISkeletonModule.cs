﻿using Microsoft.Extensions.DependencyInjection;
using Skeleton.Base;

namespace Skeleton.Interfaces
{
    public interface ISkeletonModule
    {
        void RegisterPages(DataTemplateDictionary dictionary);
        void ConfigureServices(ServiceCollection services);
    }
}
