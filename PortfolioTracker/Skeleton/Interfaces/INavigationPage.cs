﻿namespace Skeleton.Interfaces
{
    public interface IPageNavigation : IPageViewModel
    {
        void NavigateTo(object obj);
        void NavigateFrom();
    }
}
