﻿using System;

namespace Skeleton.Messengers
{
    public class SkeletonMessage
    {
        public MessageType Type { get; }
        public string Action { get; }
        public string Message { get; }
        public Exception Exception { get; }
        public DateTime TimeStamp { get; private set; }

        public SkeletonMessage(MessageType type, string message, string action)
        {
            Type = type;

            Action = action;        

            Message = message;

            TimeStamp = DateTime.Now;
        }

        public SkeletonMessage(MessageType type, string message, string action, Exception exception) : this(type, message, action)
        {
            Exception = exception;
        }

        public SkeletonMessage(MessageType type, string message, string action, DateTime timeStamp)
        {
            Type = type;

            Action = action;      
            
            Message = message;

            TimeStamp = timeStamp;
        }

        public SkeletonMessage(MessageType type, string message, string action, DateTime timeStamp, Exception exception) : this(type, message, action, timeStamp)
        {
            Exception = exception;
        }
    }
}