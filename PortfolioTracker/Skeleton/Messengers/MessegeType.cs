﻿namespace Skeleton.Messengers
{
    public enum MessageType
    {
        Success = 0,
        Warning = 1,
        Error = 2
    }
}
