﻿using System;
using System.Collections.ObjectModel;
using Skeleton.Communications;

namespace Skeleton.Messengers
{
    public class SkeletonMessenger
    {
        public ObservableCollection<SkeletonMessage> Messages { get; }

        private readonly PubSubHandler<SkeletonMessage> _pubSubHandler;
        public SkeletonMessenger()
        {
            Messages = new ObservableCollection<SkeletonMessage>();

            _pubSubHandler = new PubSubHandler<SkeletonMessage>();
        }

        public virtual EventDisposable Subscribe(Action<SkeletonMessage> subscriber)
        {
            return _pubSubHandler.Subscribe(subscriber);
        }

        public void Publish(SkeletonMessage item)
        {
            Messages.Add(item);

            _pubSubHandler.Publish(item);
        }
    }
}
