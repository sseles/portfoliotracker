﻿using System.Collections.Generic;
using AlphaVantageApi;
using FmpApi;
using ApiBase.Enums;
using ApiBase.Interfaces;
using ApiBase.Interfaces.FD;
using ApiBase.Interfaces.Models;
using DatabaseModels.Users;
using ImporterServices;

namespace StockService
{
    public class StockServiceManager
    {
        public IFundamentalData FundamentalData { get; private set; }

        private readonly Dictionary<int, IStockService> _services;

        public StockServiceManager()
        {
            _services = new Dictionary<int, IStockService>();
        }

        /// <summary>
        /// Register to the institution service to request data
        /// </summary>
        /// <param name="serviceKeys">Selection institution that provided the data and the API key for authentication</param>
        /// <param name="config">Set function with specific API call</param>
        public void RegisterService(IEnumerable<ServiceKey> serviceKeys, IServiceConfig config)
        {
            CleanServices();

            foreach (ServiceKey key in serviceKeys)
            {
                switch (key.ServiceTypeId)
                {
                    case ServiceInstitution.AlphaVantageApi:
                        IStockService alpha = new AlphaVantageService(key.ApiKey);
                        _services.Add(key.Id, alpha);
                        break;
                    case ServiceInstitution.FinancialModelingPrepApi:
                        IStockService fmp = new FmpService(key.ApiKey);
                        _services.Add(key.Id, fmp);
                        break;
                }
            }

            ConfigureService(config);
        }

        public void ConfigureService(IServiceConfig config)
        {
            FundamentalData fundamentalData = new FundamentalData();

            fundamentalData.SetSearchService(_services[config.SearchServiceKey].SearchCompaniesService);
            fundamentalData.SetOverviewService(_services[config.OverviewServiceKey].CompanyOverviewService);
            fundamentalData.SetQuote(_services[config.QuoteServiceKey].QuoteService);

            FundamentalData = fundamentalData;
        }

        private void CleanServices()
        {
            foreach (var stockService in _services)
            {
                stockService.Value.Dispose();
            }

            _services.Clear();
        }
    }
}
