﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ApiBase.Interfaces.FD;
using ApiBase.Interfaces.Models;

namespace ImporterServices
{
    public class FundamentalData : IFundamentalData
    {
        private ISearchCompaniesService _searchService;
        private ICompanyOverviewService _overviewService;
        private IQuoteService _quoteService;

        public void SetSearchService(ISearchCompaniesService searchService)
        {
            _searchService = searchService;
        }

        public void SetOverviewService(ICompanyOverviewService overviewService)
        {
            _overviewService = overviewService;
        }

        public void SetQuote(IQuoteService quoteService)
        {
            _quoteService = quoteService;
        }

        public Task<IEnumerable<ISymbolSearchResult>> SearchCompanies(string keywords, CancellationToken? token)
        {
            return _searchService.SearchCompanies(keywords, token);
        }

        public Task<ICompanyQuote> GetQuote(string symbol, CancellationToken? token)
        {
            return _quoteService.GetQuote(symbol, token);
        }

        public Task<IEnumerable<ICompanyQuote>> GetQuotes(params string[] symbols)
        {
            return _quoteService.GetQuotes(symbols);
        }

        public Task<IEnumerable<ICompanyQuote>> GetQuotes(CancellationToken? token, params string[] symbols)
        {
            return _quoteService.GetQuotes(token, symbols);
        }

        public Task<ICompanyOverview> GetCompanyOverview(string symbol, CancellationToken? token)
        {
            return _overviewService.GetCompanyOverview(symbol, token);
        }

        public Task<IEnumerable<ICompanyOverview>> GetCompanyOverviews(params string[] symbols)
        {
            return _overviewService.GetCompanyOverviews(symbols);
        }

        public Task<IEnumerable<ICompanyOverview>> GetCompanyOverviews(CancellationToken? token, params string[] symbols)
        {
            return _overviewService.GetCompanyOverviews(token, symbols);
        }
    }
}
