﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using ApiBase.Interfaces.Models;
using DatabaseModels.Users;
using DatabaseRepositories;
using Microsoft.EntityFrameworkCore;
using PortfolioTracker.Model.Dividends;
using PortfolioTracker.Model.Stocks;
using PortfolioTracker.Model.Users;
using Skeleton.Base;
using Skeleton.Timers;
using SqlLiteDatabase.Repositories.Users;
using StockService;

namespace PortfolioTracker.Services
{
    public class ActiveUserService : SkeletonPropertyChanged
    {
        public SettingsModel SettingsModel { get; set; }
        public ApiSettingsModel ApiSettingsModel { get; set; }
        public PortfolioModel PortfolioModel { get; private set; }
        public DividendPortfolioModel DividendModel { get; private set; }

        public string UserId
        {
            get => _userId;
            private set
            {
                _userId = value;
                OnPropertyChanged();
            }
        }

        private string _userId;

        private readonly UserStockRepository _stockRepository;

        private readonly StockServiceManager _serviceManager;

        private readonly SkeletonTimer _updatePricesTimer;

        public ActiveUserService(SqlLiteDatabaseContext dbContext, StockServiceManager importerService)
        {
            _stockRepository = new UserStockRepository(dbContext);

            _updatePricesTimer = new SkeletonTimer(Callback)
            {
                Interval = 60000
            };

            _serviceManager = importerService;
        }

        public async Task Initialize(User user)
        {
            UserId = user.UserId;

            SettingsModel = new SettingsModel(user.UserSetting);

            ApiSettingsModel = new ApiSettingsModel(user.ServiceKeys, user.ServiceConfig);

            PortfolioModel = new PortfolioModel();

            DividendModel = new DividendPortfolioModel();

            List<UserStock> stocks = await _stockRepository
                .Get(p => p.UserId == UserId)
                .OrderBy(p => p.Symbol)
                .ToListAsync();

            PortfolioModel.InitializeStocks(stocks);

            DividendModel.InitializeStocks(stocks);

            _updatePricesTimer.Start();
        }

        public async void AddTrade(TradeModel tradeStock)
        {
            PortfolioStockModel portfolioStock = PortfolioModel.Stocks.SingleOrDefault(p => p.Symbol == tradeStock.Symbol);

            UserStock stock;

            if (portfolioStock == null)
            {
                stock = new UserStock()
                {
                    UserId = UserId,
                    Symbol = tradeStock.Symbol,
                    AveragePrice = tradeStock.Price,
                    TotalQuantity = tradeStock.Quantity,
                    HasDividends = tradeStock.HasDividends
                };

                stock.Trades.Add(new StockTrade()
                {
                    Price = tradeStock.Price,
                    Quantity = tradeStock.Quantity,
                    Buy = tradeStock.Buy,
                });

                _stockRepository.Insert(stock);

                await _stockRepository.SaveChangesAsync();
            }
            else
            {
                stock = await _stockRepository.GetById(portfolioStock.DatabaseId);

                if (tradeStock.Buy)
                {
                    stock.AveragePrice = stock.AveragePrice * stock.TotalQuantity + tradeStock.Price * tradeStock.Quantity;
                    stock.TotalQuantity += tradeStock.Quantity;
                    stock.AveragePrice /= stock.TotalQuantity;
                }
                else
                {
                    stock.TotalQuantity -= tradeStock.Quantity;
                }

                StockTrade stockTrade = new StockTrade()
                {
                    UserStockId = stock.Id,
                    Price = tradeStock.Price,
                    Quantity = tradeStock.Quantity,
                    Buy = tradeStock.Buy,
                };

                stock.Trades.Add(stockTrade);

                _stockRepository.Update(stock);

                await _stockRepository.SaveChangesAsync();
            }

            PortfolioModel.AddTrade(stock);

            if (stock.HasDividends)
            {
                DividendModel.AddTrade(stock);
            }
        }

        public bool CanTrade(TradeModel tradeStock)
        {
            if (tradeStock.Buy == true) return true;

            PortfolioStockModel stock = PortfolioModel.Stocks.SingleOrDefault(p => p.Symbol == tradeStock.Symbol);

            if (stock.Quantity >= tradeStock.Quantity)
            {
                return true;
            }

            return false;
        }

        public void StartUpdate()
        {
            if (SettingsModel.AutoUpdate)
            {
                _updatePricesTimer.Start();
            }
        }

        public void StopUpdate()
        {
            _updatePricesTimer.Stop();
        }

        public async Task ManualUpdateStockPrices(CancellationToken? token)
        {
            await UpdatePrices(token);
        }

        public async Task ManualUpdateStockData(CancellationToken? token)
        {
            await UpdateData(token);
        }

        private async void Callback(object state)
        {
            await UpdatePrices(null);
        }

        public void UpdateSettings()
        {
            StopUpdate();

            _updatePricesTimer.Interval = SettingsModel.AutoUpdatePeriod * 1000;

            _serviceManager.RegisterService(ApiSettingsModel.ApiKeys,ApiSettingsModel);
        }

        private async Task UpdatePrices(CancellationToken? token)
        {
            if (PortfolioModel.GetSymbols().Length <= 0) return;

            IEnumerable<ICompanyQuote> quotes = await _serviceManager.FundamentalData.GetQuotes(token, PortfolioModel.GetSymbols());

            Dispatcher.CurrentDispatcher.Invoke(
                () =>
                {
                    PortfolioModel.UpdatePrices(quotes);
                }, DispatcherPriority.Normal);
        }

        private async Task UpdateData(CancellationToken? token)
        {
            if (PortfolioModel.GetSymbols().Length <= 0) return;

            IEnumerable<ICompanyOverview> overview = await _serviceManager.FundamentalData.GetCompanyOverviews(token, PortfolioModel.GetSymbols());
        }
    }
}
