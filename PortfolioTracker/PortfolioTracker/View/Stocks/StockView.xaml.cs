﻿using System.Windows;
using System.Windows.Controls;

namespace PortfolioTracker.View.Stocks
{
    public partial class StockView : UserControl
    {
        public StockView()
        {
            InitializeComponent();
        }

        private void Expander_OnExpanded(object sender, RoutedEventArgs e)
        {
            Expander expander = (Expander)sender;

            if (expander.IsExpanded)
            {
                 ExpanderColumn.Width= new GridLength(1, GridUnitType.Star);
            }
            else
            {
                ExpanderColumn.Width = new GridLength(25, GridUnitType.Pixel);
            }
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            Expander.IsExpanded = true;
        }
    }
}
