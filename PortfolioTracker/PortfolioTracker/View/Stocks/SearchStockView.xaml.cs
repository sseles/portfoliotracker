﻿using System.Windows;
using System.Windows.Controls;

namespace PortfolioTracker.View.Stocks
{
    public partial class SearchStockView : UserControl
    {
        public SearchStockView()
        {
            InitializeComponent();
        }

        private void Expander_OnExpanded(object sender, RoutedEventArgs e)
        {
            Expander expander = (Expander)sender;

            if (expander.IsExpanded)
            {
                Popup.IsOpen = true;
            }
            else
            {
                Popup.IsOpen = false;
            }
        }
    }
}
