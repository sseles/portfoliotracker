﻿using System.Windows.Controls;
using System.Windows;

namespace PortfolioTracker.View.Users
{
    public partial class SideNavigationView : UserControl
    {
        public SideNavigationView()
        {
            InitializeComponent();
        }
        private void ButtonOpenMenu_Click(object sender, RoutedEventArgs e)
        {
            ButtonCloseMenu.Visibility = Visibility.Visible;

            ButtonOpenMenu.Visibility = Visibility.Collapsed;
        }

        private void ButtonCloseMenu_Click(object sender, RoutedEventArgs e)
        {
            ButtonCloseMenu.Visibility = Visibility.Collapsed;

            ButtonOpenMenu.Visibility = Visibility.Visible;
        }

    }
}
