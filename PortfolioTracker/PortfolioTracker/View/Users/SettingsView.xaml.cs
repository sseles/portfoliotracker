﻿using System.Windows;
using System.Windows.Controls;

namespace PortfolioTracker.View.Users
{
    public partial class SettingsView : UserControl
    {
        public SettingsView()
        {
            InitializeComponent();
        }

        private void OnStartCheckbox_OnClick(object sender, RoutedEventArgs e)
        {
            CheckBox checkBox = sender as CheckBox;

            if (checkBox.IsChecked == true)
            {
                LabelBox.IsEnabled = true;
            }
            else
            {
                LabelBox.IsEnabled = false;
            }
        }
    }
}
