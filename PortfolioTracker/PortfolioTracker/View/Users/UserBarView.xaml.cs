﻿using System.Windows;
using System.Windows.Controls;

namespace PortfolioTracker.View.Users
{
    public partial class UserBarView : UserControl
    {
        public UserBarView()
        {
            InitializeComponent();
        }

        private void ButtonDropDownMenu_OnClick(object sender, RoutedEventArgs e)
        {
            Popup.IsOpen = !Popup.IsOpen;
        }
    }
}
