﻿using PortfolioTracker.Model.Dividends;
using PortfolioTracker.Services;
using Skeleton;

namespace PortfolioTracker.ViewModel.Dividends
{
    public class DividendPortfolioViewModel : SkeletonViewModel
    {
        public DividendPortfolioModel Model { get; set; }

        public DividendPortfolioViewModel(ActiveUserService activeUser)
        {
            Model = activeUser.DividendModel;
        }
    }
}
