﻿using PortfolioTracker.Model.Dividends;
using PortfolioTracker.Services;
using Skeleton;
using Skeleton.Interfaces;

namespace PortfolioTracker.ViewModel.Dividends
{
    public class DividendCalendarViewModel : SkeletonViewModel, IPageNavigation 
    {
        public DividendPortfolioModel Model { get; set; }

        public DividendCalendarViewModel(ActiveUserService activeUser)
        {
            Model = activeUser.DividendModel;
        }

        public void NavigateTo(object obj)
        {
        }

        public void NavigateFrom()
        {
        }
    }
}
