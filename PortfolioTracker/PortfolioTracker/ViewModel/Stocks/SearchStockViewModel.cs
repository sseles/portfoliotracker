﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Input;
using ApiBase.Interfaces.Models;
using PortfolioTracker.Model.Stocks;
using Skeleton;
using Skeleton.Navigation;
using Skeleton.Popups;
using StockService;

namespace PortfolioTracker.ViewModel.Stocks
{
    public class SearchStockViewModel : SkeletonViewModel
    {
        public ICommand SearchCommand { get; set; }
        public ICommand ClearCommand { get; set; }
        public ICommand SelectCommand { get; set; }

        public SearchStockModel Model { get; set; }

        private readonly SkeletonNavigation _navigation;

        private readonly StockServiceManager _stockService;

        public SearchStockViewModel(StockServiceManager stockService, SkeletonNavigation navigation)
        {
            _stockService = stockService;

            _navigation = navigation;

            Model = new SearchStockModel();

            SearchCommand = new SkeletonCommand(SearchAction, CanSearch);

            ClearCommand = new SkeletonCommand(ClearAction);

            SelectCommand = new SkeletonCommand(SelectAction);
        }

        public bool CanSearch(object obj)
        {
            if (string.IsNullOrWhiteSpace(Model.Keyword))
            {
                Model.SearchResults = null;

                return false;
            }

            return true;
        }

        public async void SearchAction(object obj)
        {
            ProgressBox progress = new ProgressBox("Fetching data");

            CancellationToken? token = progress.Show();

            IEnumerable<ISymbolSearchResult> searchResult = await _stockService.FundamentalData.SearchCompanies(Model.Keyword, token);

            Model.SearchResults = searchResult;

            progress.Finish();
        }

        private void ClearAction(object obj)
        {
            Model.Keyword = string.Empty;

            Model.SearchResults = null;
        }

        private void SelectAction(object obj)
        {
            _navigation.Navigate<StockViewModel>(obj);
        }
    }
}
