﻿using System.Windows.Input;
using PortfolioTracker.Model.Stocks;
using PortfolioTracker.Services;
using Skeleton;
using Skeleton.Interfaces;
using Skeleton.Navigation;

namespace PortfolioTracker.ViewModel.Stocks
{
    public class PortfolioViewModel : SkeletonViewModel, IPageNavigation
    {
        public PortfolioModel Model { get; set; }

        public ICommand DetailsCommand { get; set; }

        private readonly SkeletonNavigation _navigation;

        public PortfolioViewModel(SkeletonNavigation navigation, ActiveUserService activeUser)
        {
            _navigation = navigation;

            Model = activeUser.PortfolioModel;

            DetailsCommand = new SkeletonCommand(DetailsAction);
        }

        private void DetailsAction(object obj)
        {
            _navigation.Navigate<StockViewModel>(obj);
        }

        public void NavigateTo(object obj)
        {
        }

        public void NavigateFrom()
        {
        }
    }
}
