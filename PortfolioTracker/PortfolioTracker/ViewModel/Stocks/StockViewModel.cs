﻿using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ApiBase.Interfaces.Models;
using CustomControls.Extensions;
using DatabaseModels.Stocks;
using DatabaseRepositories;
using DatabaseRepositories.Repositories.Stocks;
using PortfolioTracker.Model.Stocks;
using PortfolioTracker.Services;
using Skeleton;
using Skeleton.Interfaces;
using Skeleton.Popups;
using StockService;
using MessageBox = Skeleton.Popups.MessageBox;

namespace PortfolioTracker.ViewModel.Stocks
{
    public class StockViewModel : SkeletonViewModel, IPageNavigation
    {
        public StockModel Model { get; set; }
        public ICommand SelectTabCommand { get; set; }
        public ICommand ClosePositionCommand { get; set; }

        private readonly CompanyOverviewRepository _overviewRepository;
        private readonly StockServiceManager _stockService;
        private readonly ActiveUserService _userService;

        public StockViewModel(StockServiceManager stockService, SkeletonContainer container, SqlLiteDatabaseContext databaseContext, ActiveUserService userService)
        {
            _overviewRepository = new CompanyOverviewRepository(databaseContext);

            _stockService = stockService;

            _userService = userService;

            Model = new StockModel();

            SelectTabCommand = new SkeletonCommand(SelectTabAction);

            ClosePositionCommand = new SkeletonCommand(ClosePositionAction, CanClosePosition);

            Model.TradeViewModel = container.RequestViewModel<TradeViewModel>() as TradeViewModel;
        }

        public void SelectTabAction(object obj)
        {
            Model.TradeViewModel.Model.SelectTab((bool)obj);
        }

        public bool CanClosePosition(object obj)
        {
            if (Model.UserStock != null)
            {
                return true;
            }

            return false;
        }

        public void ClosePositionAction(object obj)
        {
            MessageBox messageBox = new MessageBox();

            string message = string.Format("Close the position [{0}]{1} for the price {2}", Model.Overview.Symbol, Model.Overview.Name, Model.Quote.Price);

            MessageBoxResult result = messageBox.ShowDialog("Are you sure?", message, MessageBoxButton.YesNo);

            if (result == MessageBoxResult.Yes)
            {
                TradeModel model = new TradeModel()
                {
                    Symbol = Model.UserStock.Symbol,
                    Price = Model.UserStock.AveragePrice,
                    Quantity = Model.UserStock.Quantity,
                    Buy = false
                };

                _userService.AddTrade(model);
            }
        }

        public async void NavigateTo(object obj)
        {
            ProgressBox progress = new ProgressBox("Fetching data");

            progress.Show();

            if (obj is string symbol)
            {
                await LoadFromService(symbol);

                await SaveToDatabase(Model.Overview);
            }

            Model.UserStock = _userService.PortfolioModel.Stocks.SingleOrDefault(p => p.Symbol == Model.Overview.Symbol);

            ClosePositionCommand.CanExecute(null);

            progress.Finish();
        }

        private async Task LoadFromService(string symbol)
        {
            Model.Overview = await _stockService.FundamentalData.GetCompanyOverview(symbol, null);

            Model.Quote = await _stockService.FundamentalData.GetQuote(symbol, null);

            TradeViewModel tradeView = Model.TradeViewModel;

            tradeView.UpdateModel(Model.Overview, Model.Quote.Price);
        }

        private async Task SaveToDatabase(ICompanyOverview overview)
        {
            CompanyOverview dbOverview = await _overviewRepository.GetById(overview.Symbol);

            if (dbOverview != null)
            {
                dbOverview.CopyFrom(overview);

                _overviewRepository.Update(dbOverview);
            }
            else
            {
                dbOverview = new CompanyOverview();

                dbOverview.CopyFrom(overview);

                _overviewRepository.Insert(dbOverview);
            }

            await _overviewRepository.SaveChangesAsync();
        }

        public void NavigateFrom()
        {
        }
    }
}
