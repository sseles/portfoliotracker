﻿using System.Windows;
using System.Windows.Input;
using ApiBase.Interfaces.Models;
using PortfolioTracker.Model.Stocks;
using PortfolioTracker.Services;
using Skeleton;
using MessageBox = Skeleton.Popups.MessageBox;

namespace PortfolioTracker.ViewModel.Stocks
{
    public class TradeViewModel : SkeletonViewModel
    {
        public TradeModel Model { get; set; }
        public ICommand TradeCommand { get; set; }

        private readonly ActiveUserService _userService;

        public TradeViewModel(ActiveUserService userService)
        {
            _userService = userService;

            Model = new TradeModel();

            TradeCommand = new SkeletonCommand(TradeAction, CanTrade);
        }

        private bool CanTrade(object obj)
        {
            return _userService.CanTrade(Model);
        }

        public void UpdateModel(ICompanyOverview overview, decimal price)
        {
            Model.Symbol = overview.Symbol;

            Model.CompanyName = overview.Name;

            Model.Currency = overview.Currency;

            Model.CurrentPrice = price;

            Model.Price = price;
        }

        private void TradeAction(object obj)
        {
            MessageBox messageBox = new MessageBox();

            string message = string.Format("Add the [{0}]{1} to the portfolio.", Model.Symbol, Model.CompanyName);

            MessageBoxResult result = messageBox.ShowDialog("Are you sure?", message, MessageBoxButton.YesNo);

            if (result == MessageBoxResult.Yes)
            {
                _userService.AddTrade(Model);
            }
        }
    }
}
