﻿using System.Linq;
using DatabaseModels.Users;
using PortfolioTracker.Properties;
using PortfolioTracker.Services;
using PortfolioTracker.ViewModel.Users;
using Skeleton;
using Skeleton.Interfaces;
using Skeleton.Navigation;
using StockService;

namespace PortfolioTracker.ViewModel
{
    public class ShellViewModel : SkeletonViewModel, IPageNavigation
    {
        public IPageViewModel CurrentPageViewModel
        {
            get => _currentPageViewModel;
            set
            {
                _currentPageViewModel = value;
                OnPropertyChanged();
            }
        }
        public IPageViewModel NavigationViewModel
        {
            get => _navigationViewModel;
            set
            {
                _navigationViewModel = value;
                OnPropertyChanged();
            }
        }
        public IPageViewModel UserBarViewModel
        {
            get => _userBarViewModel;
            set
            {
                _userBarViewModel = value;
                OnPropertyChanged();
            }
        }

        private IPageViewModel _currentPageViewModel;

        private IPageViewModel _navigationViewModel;

        private IPageViewModel _userBarViewModel;

        private readonly SkeletonNavigation _navigation;

        private readonly SkeletonContainer _container;

        private readonly ActiveUserService _userService;

        private readonly StockServiceManager _stockService;

        public ShellViewModel(SkeletonContainer container, StockServiceManager stockService, SkeletonNavigation navigation, ActiveUserService userService)
        {
            _container = container;

            _userService = userService;

            _stockService = stockService;

            _navigation = navigation;

            _navigation.AddDomain(DomainName.PORTFOLIO_TRACKER_DOMAIN);

            _navigation.Subscribe(NavigationOnPageNavigated, DomainName.PORTFOLIO_TRACKER_DOMAIN);

            NavigationViewModel = container.RequestViewModel<SideNavigationViewModel>();
        }

        private void NavigationOnPageNavigated(PageNavigatedEventArgs e)
        {
            CurrentPageViewModel = e.CurrentPage;
        }

        public async void NavigateTo(object obj)
        {
            _navigation.SelectDomain(DomainName.PORTFOLIO_TRACKER_DOMAIN);

            User user = obj as User;

            _stockService.RegisterService(user.ServiceKeys, user.ServiceConfig);

            await _userService.Initialize(user);

            CurrentPageViewModel = _container.RequestViewModel<DashboardViewModel>();

            NavigationViewModel = _container.RequestViewModel<SideNavigationViewModel>();

            UserBarViewModel = _container.RequestViewModel<UserBarViewModel>();
        }

        public void NavigateFrom()
        {
        }
    }
}