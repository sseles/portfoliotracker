﻿using Skeleton;
using Skeleton.Navigation;
using System.Windows.Input;
using PortfolioTracker.ViewModel.Dividends;
using PortfolioTracker.ViewModel.Stocks;

namespace PortfolioTracker.ViewModel.Users
{
    public class SideNavigationViewModel : SkeletonViewModel
    {
        public ICommand DividendCalendarCommand { get; set; }
        public ICommand PortfolioCommand { get; set; }
        public ICommand DashboardCommand { get; set; }
        public ICommand LogoutCommand { get; set; }

        private readonly SkeletonNavigation _navigation;

        public SideNavigationViewModel(SkeletonNavigation navigation)
        {
            _navigation = navigation;

            DividendCalendarCommand = new SkeletonCommand(NavigateToDividendCalendar);

            PortfolioCommand = new SkeletonCommand(NavigateToPortfolio);

            DashboardCommand = new SkeletonCommand(NavigateToDashboard);

            LogoutCommand = new SkeletonCommand(Logout);
        }

        private void NavigateToPortfolio(object obj)
        {
            _navigation.Navigate<PortfolioViewModel>();
        }

        private void NavigateToDividendCalendar(object obj)
        {
            _navigation.Navigate<DividendCalendarViewModel>();
        }

        private void NavigateToDashboard(object obj)
        {
            _navigation.Navigate<DashboardViewModel>();
        }

        private void Logout(object obj)
        {
            _navigation.SelectDomain(SkeletonNavigation.DEFAULT_DOMAIN);
        }
    }
}
