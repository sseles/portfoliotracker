﻿using PortfolioTracker.Services;
using PortfolioTracker.ViewModel.Dividends;
using PortfolioTracker.ViewModel.Stocks;
using Skeleton;
using Skeleton.Interfaces;

namespace PortfolioTracker.ViewModel.Users
{
    public class DashboardViewModel : SkeletonViewModel, IPageNavigation
    {
        public IPageViewModel ViewModelA
        {
            get => _viewModelA;
            set
            {
                _viewModelA = value;
                OnPropertyChanged();
            }
        }
        public IPageViewModel ViewModelB
        {
            get => _viewModelB;
            set
            {
                _viewModelB = value;
                OnPropertyChanged();
            }
        }

        public IPageViewModel ViewModelC
        {
            get => _viewModelC;
            set
            {
                _viewModelC = value;
                OnPropertyChanged();
            }
        }

        private IPageViewModel _viewModelA;

        private IPageViewModel _viewModelB;

        private IPageViewModel _viewModelC;

        private readonly ActiveUserService _userService;

        public DashboardViewModel(SkeletonContainer container, ActiveUserService userService)
        {
            _userService = userService;

            ViewModelA = container.RequestViewModel<SearchStockViewModel>();

            ViewModelB = container.RequestViewModel<PortfolioViewModel>();

            ViewModelC = container.RequestViewModel<DividendCalendarViewModel>();
        }

        public void NavigateTo(object obj)
        {
            _userService.StartUpdate();
        }

        public void NavigateFrom()
        {
            _userService.StopUpdate();
        }
    }
}
