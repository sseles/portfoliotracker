﻿using System.Threading;
using System.Windows.Input;
using PortfolioTracker.Services;
using Skeleton;
using Skeleton.Navigation;
using Skeleton.Popups;

namespace PortfolioTracker.ViewModel.Users
{
    public class UserBarViewModel : SkeletonViewModel
    {
        public ICommand UpdateStockPricesCommand { get; set; }
        public ICommand UpdateStockDataCommand { get; set; }
        public ICommand OpenApiSettingsCommand { get; set; }
        public ICommand OpenSettingsCommand { get; set; }

        public string UserName
        {
            get => _userName;
            set
            {
                _userName = value;
                OnPropertyChanged();
            }
        }

        private string _userName;

        private readonly SkeletonNavigation _navigation;

        private readonly ActiveUserService _userService;

        public UserBarViewModel(SkeletonNavigation navigation, ActiveUserService userService)
        {
            _navigation = navigation;
            
            _userService = userService;

            UserName = _userService.UserId;

            UpdateStockDataCommand = new SkeletonCommand(UpdateStockDataAction);

            UpdateStockPricesCommand = new SkeletonCommand(UpdateStockPricesAction);

            OpenApiSettingsCommand = new SkeletonCommand(OpenApiSettingsAction);

            OpenSettingsCommand = new SkeletonCommand(OpenSettingsAction);
        }

        private async void UpdateStockDataAction(object obj)
        {
            ProgressBox progress = new ProgressBox("Updating Data! This may take a while, depending on your API service.");

            CancellationToken? token = progress.Show(true);

            await _userService.ManualUpdateStockData(token);

            progress.Finish();
        }

        private async void UpdateStockPricesAction(object obj)
        {
            ProgressBox progress = new ProgressBox("Updating Data! This may take a while, depending on your API service.");

            CancellationToken? token = progress.Show(true);

            await _userService.ManualUpdateStockPrices(token);

            progress.Finish();
        }

        private void OpenApiSettingsAction(object obj)
        {
            _navigation.Navigate<ApiSettingsViewModel>();
        }

        private void OpenSettingsAction(object obj)
        {
            _navigation.Navigate<SettingsViewModel>();
        }
    }
}
