﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using DatabaseModels.Users;
using DatabaseRepositories;
using PortfolioTracker.Model.Users;
using PortfolioTracker.Services;
using Skeleton;
using Skeleton.Interfaces;
using Skeleton.Popups;
using SqlLiteDatabase.Repositories.Users;
using MessageBox = Skeleton.Popups.MessageBox;

namespace PortfolioTracker.ViewModel.Users
{
    public class ApiSettingsViewModel : SkeletonViewModel, IPageNavigation
    {
        public ApiSettingsModel Model { get; set; }

        public ICommand DeleteCommand { get; set; }
        public ICommand ApplyCommand { get; set; }
        public ICommand AddCommand { get; set; }

        private readonly ServiceConfigRepository _configRepository;
        private readonly KeyRepository _keyRepository;
        private readonly ActiveUserService _userService;

        public ApiSettingsViewModel(ActiveUserService userService, SqlLiteDatabaseContext databaseContext)
        {
            _userService = userService;

            _configRepository = new ServiceConfigRepository(databaseContext);

            _keyRepository = new KeyRepository(databaseContext);

            Model = _userService.ApiSettingsModel;

            DeleteCommand = new SkeletonCommand(DeleteAction);

            ApplyCommand = new SkeletonCommand(ApplyAction);

            AddCommand = new SkeletonCommand(AddAction, CanAdd);
        }

        private async void DeleteAction(object obj)
        {
            if (Model.ApiKeys.Count <= 1)
            {
                MessageBox errorMessage = new MessageBox();

                errorMessage.ShowDialog("Error!", "At least one key is required for the application to continue functioning!");

                return;
            }

            MessageBox warningMessage = new MessageBox();

            MessageBoxResult result = warningMessage.ShowDialog("Warning!", "This will completely delete the key and it will no longer be recoverable! Are you sure you want to proceed?", MessageBoxButton.YesNo);

            if (result == MessageBoxResult.Yes)
            {
                ServiceKey key = Model.ApiKeys.Find(p => p.Id == (int)obj);

                _keyRepository.Delete(key);

                await _keyRepository.SaveChangesAsync();

                Model.ApiKeys.Remove(key);
            }
        }

        private async void ApplyAction(object obj)
        {
            ProgressBox progress = new ProgressBox("Updating...");

            progress.Show();

            ServiceConfig config = await _configRepository.GetById(_userService.UserId);

            config.SearchServiceKey = Model.SearchServiceKey;
            config.QuoteServiceKey = Model.QuoteServiceKey;
            config.OverviewServiceKey = Model.OverviewServiceKey;

            _configRepository.Update(config);
            await _configRepository.SaveChangesAsync();
            _userService.UpdateSettings();

            Model.CleanNewKeyData();

            progress.Finish();
        }

        private void AddAction(object obj)
        {
            _keyRepository.Insert(Model.NewKey);

            Model.ApiKeys.Add(Model.NewKey);

            ApplyAction(null);
        }

        private bool CanAdd(object obj)
        {
            if (Model.ApiKeys.Exists(p => p.Name == Model.NewKey.Name)) return false;

            if (string.IsNullOrWhiteSpace(Model.NewKey.Name)) return false;

            if (string.IsNullOrWhiteSpace(Model.NewKey.ApiKey)) return false;

            if (Model.NewKey.ServiceTypeId == 0) return false;

            return true;
        }

        public void NavigateTo(object obj)
        {
        }

        public void NavigateFrom()
        {
        }
    }
}
