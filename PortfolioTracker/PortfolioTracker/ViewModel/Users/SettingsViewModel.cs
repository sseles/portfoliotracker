﻿using System.Windows.Input;
using DatabaseModels.Users;
using DatabaseRepositories;
using PortfolioTracker.Model.Users;
using PortfolioTracker.Services;
using Skeleton;
using Skeleton.Interfaces;
using Skeleton.Popups;
using SqlLiteDatabase.Repositories.Users;

namespace PortfolioTracker.ViewModel.Users
{
    public class SettingsViewModel : SkeletonViewModel, IPageNavigation
    {
        public SettingsModel Model { get; set; }

        public ICommand ApplyCommand { get; set; }

        private readonly UserSettingsRepository _settingsRepository;
        private readonly ActiveUserService _userService;

        public SettingsViewModel(ActiveUserService userService, SqlLiteDatabaseContext databaseContext)
        {
            _settingsRepository = new UserSettingsRepository(databaseContext);

            _userService = userService;

            Model = userService.SettingsModel;

            ApplyCommand = new SkeletonCommand(ApplyAction);
        }

        private async void ApplyAction(object obj)
        {
            UserSetting settings = await _settingsRepository.GetById(_userService.UserId);

            settings.AutoUpdate = Model.AutoUpdate;
            settings.OnStartUpdate = Model.OnStartUpdate;
            settings.AutoUpdatePeriod = Model.AutoUpdatePeriod;

            _settingsRepository.Update(settings);
            await _settingsRepository.SaveChangesAsync();
            _userService.UpdateSettings();

            MessageBox messageBox = new MessageBox();

            messageBox.ShowDialog("Success!", "Application updated and data saved.");
        }

        public void NavigateTo(object obj)
        {
        }

        public void NavigateFrom()
        {
        }
    }
}
