﻿using DatabaseRepositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using PortfolioTracker.Services;
using PortfolioTracker.ViewModel;
using PortfolioTracker.ViewModel.Dividends;
using PortfolioTracker.ViewModel.Stocks;
using PortfolioTracker.ViewModel.Users;
using Skeleton.Base;
using Skeleton.Interfaces;
using StockService;

namespace PortfolioTracker
{
    public class PortfolioTrackerModule : ISkeletonModule
    {
        public void RegisterPages(DataTemplateDictionary dictionary)
        {
            dictionary.AddPage<ShellViewModel>();

            dictionary.AddPage<SideNavigationViewModel>();

            dictionary.AddPage<UserBarViewModel>();

            dictionary.AddPage<SettingsViewModel>(); 

            dictionary.AddPage<ApiSettingsViewModel>();

            dictionary.AddPage<DashboardViewModel>();

            dictionary.AddPage<PortfolioViewModel>();

            dictionary.AddPage<SearchStockViewModel>();

            dictionary.AddPage<StockViewModel>();

            dictionary.AddPage<TradeViewModel>();

            dictionary.AddPage<DividendPortfolioViewModel>();

            dictionary.AddPage<DividendCalendarViewModel>();
        }

        public void ConfigureServices(ServiceCollection services)
        {
            services.AddDbContext<SqlLiteDatabaseContext>(options =>
                options.UseSqlite(), ServiceLifetime.Singleton);

            services.AddSingleton<StockServiceManager>();

            services.AddSingleton<ActiveUserService>();
        }
    }
}
