﻿using System.ComponentModel;
using Skeleton;

namespace PortfolioTracker.Model.Stocks
{
    public class PortfolioStockModel : SkeletonModel
    {
        public int DatabaseId { get; private set; }
        public string Symbol
        {
            get => _symbol;
            set
            {
                _symbol = value;
                OnPropertyChanged();
            }
        }
        public string CompanyName
        {
            get => _companyName;
            set
            {
                _companyName = value;
                OnPropertyChanged();
            }
        }
        public string Currency
        {
            get => _currency;
            set
            {
                _currency = value;
                OnPropertyChanged();
            }
        }
        public decimal CurrentPrice
        {
            get => _currentPrice;
            set
            {
                _currentPrice = value;
                OnPropertyChanged();
            }
        }
        public decimal Quantity
        {
            get => _quantity;
            set
            {
                _quantity = value;
                OnPropertyChanged();
            }
        }
        public decimal AveragePrice
        {
            get => _averagePrice;
            set
            {
                _averagePrice = value;
                OnPropertyChanged();
            }
        }
        public decimal MarketValue
        {
            get => _marketValue;
            set
            {
                _marketValue = value;
                OnPropertyChanged();
            }
        }
        public decimal PaidPrice
        {
            get => _paidPrice;
            set
            {
                _paidPrice = value;
                OnPropertyChanged();
            }
        }

        public PortfolioStockModel(int databaseId)
        {
            DatabaseId = databaseId;

            PropertyChanged += PortfolioStockModel_PropertyChanged;
        }

        private void PortfolioStockModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(CurrentPrice) || 
                e.PropertyName == nameof(AveragePrice) || 
                e.PropertyName == nameof(Quantity))
            {
                PaidPrice = AveragePrice * Quantity;

                MarketValue = CurrentPrice * Quantity;
            }
        }

        private decimal _quantity;
        private decimal _averagePrice;
        private decimal _currentPrice;
        private decimal _marketValue;
        private decimal _paidPrice;
        private string _symbol;
        private string _companyName;
        private string _currency;
    }
}
