﻿using Skeleton;

namespace PortfolioTracker.Model.Stocks
{
    public class TradeModel : SkeletonModel
    {
        public bool BuyTabSelected
        {
            get => _buyTabSelected;
            set
            {
                _buyTabSelected = value;
                OnPropertyChanged();
            }
        }
        public bool SellTabSelected
        {
            get => _sellTabSelected;
            set
            {
                _sellTabSelected = value;
                OnPropertyChanged();
            }
        }

        public string Symbol
        {
            get => _symbol;
            set
            {
                _symbol = value;
                OnPropertyChanged();
            }
        }
        public string CompanyName
        {
            get => _companyName;
            set
            {
                _companyName = value;
                OnPropertyChanged();
            }
        }
        public string Currency
        {
            get => _currency;
            set
            {
                _currency = value;
                OnPropertyChanged();
            }
        }
        public decimal CurrentPrice
        {
            get => _currentPrice;
            set
            {
                _currentPrice = value;
                OnPropertyChanged();
            }
        }
        public decimal Quantity
        {
            get => _quantity;
            set
            {
                _quantity = value;
                OnPropertyChanged();
            }
        }
        public decimal Price
        {
            get => _price;
            set
            {
                _price = value;
                OnPropertyChanged();
            }
        }
        public bool Buy
        {
            get => _buy;
            set
            {
                _buy = value;
                OnPropertyChanged();
            }
        }
        public bool HasDividends
        {
            get => _hasDividends;
            set
            {
                _hasDividends = value;
                OnPropertyChanged();
            }
        }

        public TradeModel()
        {
            Buy = true;
        }

        public void SelectTab(bool isBuy)
        {
            BuyTabSelected = isBuy;

            SellTabSelected = !isBuy;

            Buy = isBuy;
        }

        private decimal _quantity;
        private decimal _price;
        private decimal _currentPrice;
        private string _symbol;
        private string _companyName;
        private string _currency;
        private bool _buyTabSelected;
        private bool _sellTabSelected;
        private bool _hasDividends;
        private bool _buy;
    }
}
