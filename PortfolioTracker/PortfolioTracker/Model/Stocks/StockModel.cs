﻿using ApiBase.Interfaces.Models;
using PortfolioTracker.ViewModel.Stocks;
using Skeleton;

namespace PortfolioTracker.Model.Stocks
{
    public class StockModel : SkeletonModel
    {
        public TradeViewModel TradeViewModel { get; set; }
        public ICompanyOverview Overview
        {
            get => _overview;
            set
            {
                _overview = value;
                OnPropertyChanged();
            }
        }
        public ICompanyQuote Quote
        {
            get => _quote;
            set
            {
                _quote = value;
                OnPropertyChanged();
            }
        }
        public PortfolioStockModel UserStock
        {
            get => _userHeldStock;
            set
            {
                _userHeldStock = value;
                OnPropertyChanged();
            }
        }

        private PortfolioStockModel _userHeldStock;
        private ICompanyOverview _overview;
        private ICompanyQuote _quote;
    }
}
