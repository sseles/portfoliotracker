﻿using System.Collections.Generic;
using ApiBase.Interfaces.Models;
using Skeleton;

namespace PortfolioTracker.Model.Stocks
{
    public class SearchStockModel : SkeletonModel
    {
        public IEnumerable<ISymbolSearchResult> SearchResults
        {
            get => _searchResults;
            set
            {
                _searchResults = value;
                if (value != null)
                {
                    IsExpanded = true;
                }
                else
                {
                    IsExpanded = false;
                }
                OnPropertyChanged();
            }
        }
        public string Keyword
        {
            get => _keyword;
            set
            {
                _keyword = value;
                OnPropertyChanged();
            }
        }
        public bool IsExpanded
        {
            get => _isExpanded;
            set
            {
                _isExpanded = value;
                OnPropertyChanged();
            }
        }

        private IEnumerable<ISymbolSearchResult> _searchResults;
        private string _keyword;
        private bool _isExpanded;
    }
}
