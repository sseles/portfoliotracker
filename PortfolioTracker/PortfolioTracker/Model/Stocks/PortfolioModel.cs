﻿using System.Collections.Generic;
using System.Linq;
using ApiBase.Interfaces.Models;
using DatabaseModels.Users;
using Skeleton;

namespace PortfolioTracker.Model.Stocks
{
    public class PortfolioModel : SkeletonModel
    {
        public List<PortfolioStockModel> Stocks { get; set; }

        private readonly HashSet<string> _symbols;

        public PortfolioModel()
        {
            Stocks = new List<PortfolioStockModel>();

            _symbols = new HashSet<string>();
        }

        public void InitializeStocks(List<UserStock> stocks)
        {
            Stocks.Clear();

            _symbols.Clear();

            foreach (UserStock stock in stocks)
            {
                AddStock(stock);
            }
        }

        public void AddTrade(UserStock stock)
        {
            if (_symbols.Contains(stock.Symbol) == false)
            {
                AddStock(stock);
            }
            else
            {
                PortfolioStockModel portfolioStock = Stocks.Single(p => p.Symbol == stock.Symbol);

                portfolioStock.Quantity = stock.TotalQuantity;

                portfolioStock.AveragePrice = stock.AveragePrice;
            }
        }

        public void UpdatePrices(IEnumerable<ICompanyQuote> quotes)
        {
            foreach (PortfolioStockModel stock in Stocks)
            {
                ICompanyQuote quote = quotes.SingleOrDefault(p => p.Symbol == stock.Symbol);

                if (quote != null)
                {
                    stock.CurrentPrice = quote.Price;
                }
            }
        }

        public string[] GetSymbols()
        {
            return _symbols.ToArray();
        }

        private void AddStock(UserStock stock)
        {
            _symbols.Add(stock.Symbol);

            PortfolioStockModel portfolioStock = new PortfolioStockModel(stock.Id)
            {
                Symbol = stock.Symbol,
                CompanyName = stock.CompanyOverview.Name,
                Currency = stock.CompanyOverview.Currency,
                AveragePrice = stock.AveragePrice,
                Quantity = stock.TotalQuantity
            };

            Stocks.Add(portfolioStock);
        }
    }
}
