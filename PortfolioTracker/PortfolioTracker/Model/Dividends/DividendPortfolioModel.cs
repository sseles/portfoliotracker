﻿using System.Collections.Generic;
using System.Linq;
using DatabaseModels.Users;
using Skeleton;

namespace PortfolioTracker.Model.Dividends
{
    public class DividendPortfolioModel : SkeletonModel
    {
        public List<DividendModel> Stocks { get; set; }

        public DividendPortfolioModel()
        {
            Stocks = new List<DividendModel>();
        }

        public void InitializeStocks(List<UserStock> stocks)
        {
            Stocks.Clear();

            foreach (UserStock stock in stocks)
            {
                AddStock(stock);
            }
        }

        public void AddTrade(UserStock stock)
        {
            if (Stocks.Exists(p => p.Symbol == stock.Symbol) == true)
            {
                UpdateQuantity(stock.Symbol, stock.Trades);
            }
            else
            {
                AddStock(stock);
            }
        }

        private void AddStock(UserStock stock)
        {
            DividendModel portfolioStock = new DividendModel()
            {
                Symbol = stock.Symbol,
                //DividendPerShare = stock.CompanyOverview.DividendPerShare,
                //DividendYield = stock.CompanyOverview.DividendYield,
                //DividendDate = stock.CompanyOverview.DividendDate,
                //ExDividendDate = stock.CompanyOverview.ExDividendDate
            };

            Stocks.Add(portfolioStock);

            UpdateQuantity(stock.Symbol, stock.Trades);
        }

        private void UpdateQuantity(string symbol, IEnumerable<StockTrade> quantities)
        {
            decimal quantity = 0;

            quantities.ToList().ForEach(p => quantity += p.Quantity);

            DividendModel portfolioStock = Stocks.SingleOrDefault(p => p.Symbol == symbol);

            portfolioStock.Quantity = quantity;
        }
    }
}
