﻿using System;
using System.ComponentModel;
using Skeleton;

namespace PortfolioTracker.Model.Dividends
{
    public class DividendModel : SkeletonModel
    {
        public string Symbol
        {
            get => _symbol;
            set
            {
                _symbol = value;
                OnPropertyChanged();
            }
        }
        public decimal Quantity
        {
            get => _quantity;
            set
            {
                _quantity = value;
                OnPropertyChanged();
            }
        }
        public decimal DividendPerShare
        {
            get => _dividendPerShare;
            set
            {
                _dividendPerShare = value;
                OnPropertyChanged();
            }
        }
        public decimal DividendYield
        {
            get => _dividendYield;
            set
            {
                _dividendYield = value;
                OnPropertyChanged();
            }
        }
        public decimal NextDividend
        {
            get => _nextDividend;
            set
            {
                _nextDividend = value;
                OnPropertyChanged();
            }
        }
        public decimal NextDividendTotal
        {
            get => _nextDividendTotal;
            set
            {
                _nextDividendTotal = value;
                OnPropertyChanged();
            }
        }
        public decimal TotalPayout
        {
            get => _totalPayout;
            set
            {
                _totalPayout = value;
                OnPropertyChanged();
            }
        }
        public DateTime ExDividendDate
        {
            get => _exDividendDate;
            set
            {
                _exDividendDate = value;
                OnPropertyChanged();
            }
        }
        public DateTime DividendDate
        {
            get => _dividendDate;
            set
            {
                _dividendDate = value;
                OnPropertyChanged();
            }
        }

        public DividendModel()
        {
            PropertyChanged += DividendModel_PropertyChanged;
        }

        private void DividendModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(NextDividend) ||
                e.PropertyName == nameof(Quantity))
            {
                NextDividendTotal = NextDividend * Quantity;
            }

            if (e.PropertyName == nameof(DividendPerShare) ||
                e.PropertyName == nameof(NextDividend) ||
                e.PropertyName == nameof(Quantity))
            {
                TotalPayout = DividendPerShare * Quantity;
            }
        }

        private string _symbol;
        private decimal _quantity;
        private decimal _dividendPerShare;
        private decimal _dividendYield;
        private decimal _nextDividend;
        private decimal _nextDividendTotal;
        private decimal _totalPayout;
        private DateTime _exDividendDate;
        private DateTime _dividendDate;
    }
}
