﻿using System.Collections.Generic;
using ApiBase.Enums;
using ApiBase.Interfaces.Models;
using DatabaseModels.Users;
using Skeleton;

namespace PortfolioTracker.Model.Users
{
    public class ApiSettingsModel : SkeletonModel, IServiceConfig
    {
        public static Dictionary<ServiceInstitution, string> Institutions { get; set; }
        public List<ServiceKey> ApiKeys { get; set; }
        public ServiceKey NewKey { get; set; }

        public int SearchServiceKey
        {
            get => _searchService;
            set
            {
                _searchService = value;
                OnPropertyChanged();
            }
        }

        public int QuoteServiceKey
        {
            get => _quoteService;
            set
            {
                _quoteService = value;
                OnPropertyChanged();
            }
        }

        public int OverviewServiceKey
        {
            get => _overviewService;
            set
            {
                _overviewService = value;
                OnPropertyChanged();
            }
        }

        private int _searchService;
        private int _quoteService;
        private int _overviewService;

        static ApiSettingsModel()
        {
            Institutions = new Dictionary<ServiceInstitution, string>(1)
            {
                {
                    ServiceInstitution.AlphaVantageApi, "Alpha Vantage"
                },
                {
                    ServiceInstitution.FinancialModelingPrepApi, "Financial Modeling Prep"
                }
            };
        }

        public ApiSettingsModel()
        {
            ApiKeys = new List<ServiceKey>();
        }

        public ApiSettingsModel(IEnumerable<ServiceKey> apiKeys, ServiceConfig config) : this()
        {
            SearchServiceKey = config.SearchServiceKey;

            QuoteServiceKey = config.QuoteServiceKey;

            OverviewServiceKey = config.OverviewServiceKey;

            foreach (ServiceKey key in apiKeys)
            {
                ApiKeys.Add(key);
            }

            NewKey = new ServiceKey()
            {
                UserId = config.UserId
            };
        }

        public void CleanNewKeyData()
        {
            string userId = NewKey.UserId;

            NewKey = new ServiceKey()
            {
                UserId = userId
            };
        }
    }
}
