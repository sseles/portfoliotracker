﻿using DatabaseModels.Users;
using Skeleton;

namespace PortfolioTracker.Model.Users
{
    public class SettingsModel : SkeletonModel
    {
        public bool OnStartUpdate
        {
            get => _onStartUpdate;
            set
            {
                _onStartUpdate = value;
                OnPropertyChanged();
            }
        }
        public bool AutoUpdate
        {
            get => _autoUpdate;
            set
            {
                _autoUpdate = value;
                OnPropertyChanged();
            }
        }
        public int AutoUpdatePeriod
        {
            get => _autoUpdatePeriod;
            set
            {
                _autoUpdatePeriod = value;
                OnPropertyChanged();
            }
        }

        public SettingsModel()
        {
        }

        public SettingsModel(UserSetting setting)
        {
            OnStartUpdate = setting.OnStartUpdate;

            AutoUpdate = setting.AutoUpdate;

            AutoUpdatePeriod = setting.AutoUpdatePeriod;
        }

        private int _autoUpdatePeriod;
        private bool _onStartUpdate;
        private bool _autoUpdate;
    }
}
