﻿using System.Linq;
using System.Windows.Input;
using ApplicationManager.Model.Users;
using ApplicationManager.Properties;
using DatabaseModels.Users;
using DatabaseRepositories;
using Skeleton;
using Skeleton.Interfaces;
using Skeleton.Messengers;
using Skeleton.Navigation;
using SqlLiteDatabase.Repositories.Users;

namespace ApplicationManager.ViewModel.Users
{
    public class LoginViewModel : SkeletonViewModel, IPageNavigation
    {
        public ICommand LoginCommand { get; set; }
        public ICommand NewUserCommand { get; set; }
        public LoginModel Model { get; set; }

        private readonly SkeletonNavigation _navigation;

        private readonly UserRepository _userRepository;

        public LoginViewModel(SqlLiteDatabaseContext dbContext, SkeletonNavigation navigation)
        {
            _userRepository = new UserRepository(dbContext);

            _navigation = navigation;

            Model = ModelSetup();

            LoginCommand = new SkeletonCommand(LoginAction);

            NewUserCommand = new SkeletonCommand(NewUserAction);
        }

        private static LoginModel ModelSetup()
        {
            if (Settings.Default.RememberMe == true)
            {
                return new LoginModel(Settings.Default.Username, Settings.Default.Password);
            }
            else
            {
                return new LoginModel();
            }
        }

        private async void LoginAction(object sender)
        {
            User user = await _userRepository.GetById(Model.UserId.ToUpper());

            RememberUser();

            if (user == null)
            {
                App.Messenger.Publish(new SkeletonMessage(MessageType.Error, "Invalid Credentials", "Login"));

                return;
            }

            if (user.Password.SequenceEqual(Model.GetPassword()))
            {
                App.Messenger.Publish(new SkeletonMessage(MessageType.Success, "Logged In", "Login"));

                _navigation.Navigate<PortfolioTracker.ViewModel.ShellViewModel>(user);
            }
            else
            {
                App.Messenger.Publish(new SkeletonMessage(MessageType.Error, "Invalid Credentials", "Login"));
            }
        }

        private void NewUserAction(object sender)
        {
            _navigation.Navigate<CreateUserViewModel>();
        }

        private void RememberUser()
        {
            if (Model.Remember)
            {
                Settings.Default.Username = Model.UserId;

                Settings.Default.Password = Model.EncryptPassword();

                Settings.Default.RememberMe = true;

                Settings.Default.Save();
            }
            else
            {
                Settings.Default.Username = string.Empty;

                Settings.Default.Password = string.Empty;

                Settings.Default.RememberMe = false;

                Settings.Default.Save();
            }
        }

        public void NavigateTo(object obj)
        {
        }

        public void NavigateFrom()
        {
        }
    }
}
