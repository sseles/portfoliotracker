﻿using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using ApplicationManager.Model.Users;
using DatabaseModels.Users;
using DatabaseRepositories;
using Skeleton;
using Skeleton.Interfaces;
using Skeleton.Messengers;
using Skeleton.Navigation;
using Skeleton.Popups;
using SqlLiteDatabase.Repositories.Users;

namespace ApplicationManager.ViewModel.Users
{
    public class CreateUserViewModel : SkeletonViewModel, IPageNavigation
    {
        public ICommand GetKeyCommand { get; set; }
        public ICommand CreateCommand { get; set; }
        public ICommand BackCommand { get; set; }

        public CreateUserModel Model { get; set; }

        private readonly SkeletonNavigation _navigation;

        private readonly UserRepository _userRepository;

        public CreateUserViewModel(SqlLiteDatabaseContext dbContext, SkeletonNavigation navigation)
        {
            _userRepository = new UserRepository(dbContext);

            _navigation = navigation;

            Model = new CreateUserModel();

            CreateCommand = new SkeletonCommand(CreateAction, CanExecute);

            BackCommand = new SkeletonCommand(BackAction);

            GetKeyCommand = new SkeletonCommand(GetKeyAction);
        }

        private bool CanExecute(object obj)
        {
            if (string.IsNullOrWhiteSpace(Model.Password))
            {
                return false;
            }

            if (string.Equals(Model.Password, Model.RepeatPassword) == false)
            {
                return false;
            }

            if (string.IsNullOrWhiteSpace(Model.ApiKey))
            {
                return false;
            }

            return true;
        }

        private async void CreateAction(object obj)
        {
            User user = await _userRepository.GetById(Model.UserId.ToUpper());

            if (user == null)
            {
                ProgressBox progress = new ProgressBox("Creating user...");

                progress.Show();

                user = new User()
                {
                    UserId = Model.UserId.ToUpper(),
                    Password = Model.GetPassword()
                };

                user.ServiceKeys.Add(new ServiceKey()
                {
                    Name = Model.Institution.ToString(),
                    ApiKey = Model.ApiKey,
                    ServiceTypeId = Model.Institution
                });

                user.UserSetting = new UserSetting()
                {
                    UserId = user.UserId,
                    AutoUpdate = false,
                    OnStartUpdate = true
                };

                _userRepository.Insert(user);

                await _userRepository.SaveChangesAsync();

                int keyId = user.ServiceKeys.First().Id;

                user.ServiceConfig = new ServiceConfig()
                {
                    OverviewServiceKey = keyId,
                    QuoteServiceKey = keyId,
                    SearchServiceKey = keyId
                };

                _userRepository.Update(user);

                await _userRepository.SaveChangesAsync();

                App.Messenger.Publish(new SkeletonMessage(MessageType.Success, "User successfully created",
                    "Create User"));

                progress.Finish();

                _navigation.Navigate<LoginViewModel>();
            }
            else
            {
                App.Messenger.Publish(new SkeletonMessage(MessageType.Error, "User already exists",
                    "Create User"));
            }
        }

        private void BackAction(object obj)
        {
            _navigation.Navigate<LoginViewModel>();
        }

        private void GetKeyAction(object obj)
        {
            ProcessStartInfo psi = new ProcessStartInfo
            {
                FileName = Model.HyperlinkUrl,
                UseShellExecute = true
            };

            Process.Start(psi);
        }

        public void NavigateTo(object obj)
        {
        }

        public void NavigateFrom()
        {
        }
    }
}
