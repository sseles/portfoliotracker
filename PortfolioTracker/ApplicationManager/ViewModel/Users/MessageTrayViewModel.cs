﻿using System.Collections.ObjectModel;
using Skeleton;
using Skeleton.Messengers;

namespace ApplicationManager.ViewModel.Users
{
    public class MessageTrayViewModel : SkeletonViewModel
    {
        public ObservableCollection<SkeletonMessage> Messages { get; set; }

        public MessageTrayViewModel(SkeletonMessenger messenger)
        {
            Messages = messenger.Messages;

            messenger.Subscribe(OnMessageUpdate);
        }

        private void OnMessageUpdate(SkeletonMessage message)
        {

        }
    }
}
