﻿using ApplicationManager.ViewModel.Users;
using Skeleton;
using Skeleton.Interfaces;
using Skeleton.Navigation;

namespace ApplicationManager.ViewModel
{
    public class ShellViewModel : SkeletonViewModel
    {
        public IPageViewModel CurrentPageViewModel
        {
            get => _currentPageViewModel;
            set
            {
                _currentPageViewModel = value;
                OnPropertyChanged();
            }
        }
        public IPageViewModel FooterViewModel
        {
            get => _footerViewModel;
            set
            {
                _footerViewModel = value;
                OnPropertyChanged();
            }
        }

        private IPageViewModel _currentPageViewModel;

        private IPageViewModel _footerViewModel;

        public ShellViewModel(SkeletonContainer container, SkeletonNavigation navigation)
        {
            FooterViewModel = container.RequestViewModel<MessageTrayViewModel>();

            navigation.Subscribe(NavigationOnPageNavigated);

            navigation.Navigate<LoginViewModel>();
        }

        private void NavigationOnPageNavigated(PageNavigatedEventArgs e)
        {
            CurrentPageViewModel = e.CurrentPage;
        }
    }
}
