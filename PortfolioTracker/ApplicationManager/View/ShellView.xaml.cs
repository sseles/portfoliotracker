﻿using System.Windows;
using ApplicationManager.ViewModel;
using Skeleton;
using Skeleton.Navigation;

namespace ApplicationManager.View
{
    public partial class ShellView : Window
    {
        public ShellView(SkeletonContainer container, SkeletonNavigation navigation)
        {
            InitializeComponent();

            DataContext = new ShellViewModel(container, navigation);
        }
    }
}
