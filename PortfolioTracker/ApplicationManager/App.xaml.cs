﻿using ApplicationManager.View;
using ApplicationManager.ViewModel.Users;
using CustomControls;
using DatabaseRepositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using PortfolioTracker;
using Skeleton;
using Skeleton.Base;
using Skeleton.Messengers;

namespace ApplicationManager
{
    public partial class App : SkeletonApplication
    {
        public static SkeletonMessenger Messenger { get; set; }

        public App()
        {
            Controls.Configuration();

            StartUp<ShellView>();
        }

        public override void RegisterPages(DataTemplateDictionary dictionary)
        {
            dictionary.AddPage<CreateUserViewModel>();

            dictionary.AddPage<LoginViewModel>();

            dictionary.AddPage<MessageTrayViewModel>();
        }

        public override void RegisterModules(SkeletonModuleBuilder builder)
        {
            builder.AddModule<PortfolioTrackerModule>();
        }

        public override void ConfigureServices(ServiceCollection services)
        {
            services.AddDbContext<SqlLiteDatabaseContext>(options =>
                options.UseSqlite(), ServiceLifetime.Singleton);

            services.AddSingleton<SkeletonMessenger>();
        }

        public override void OnStartup(ServiceProvider serviceProvider)
        {
            serviceProvider.GetService<SqlLiteDatabaseContext>()
                .Database.Migrate();

            Messenger = serviceProvider.GetService<SkeletonMessenger>();
        }
    }
}
