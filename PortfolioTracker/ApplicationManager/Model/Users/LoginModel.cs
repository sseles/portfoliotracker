﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Skeleton;

namespace ApplicationManager.Model.Users
{
    public class LoginModel : SkeletonModel
    {
        public string UserId
        {
            get => _userId;
            set
            {
                _userId = value;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get => _password;
            set
            {
                _password = value;
                OnPropertyChanged();
            }
        }

        public bool Remember
        {
            get => _remember;
            set
            {
                _remember = value;
                OnPropertyChanged();
            }
        }

        public LoginModel()
        {
            Remember = false;
        }

        public LoginModel(string username, string encryptedPassword)
        {
            Remember = true;

            UserId = username;

            Password = DecryptPassword(encryptedPassword);
        }

        public byte[] GetPassword()
        {
            byte[] data = Encoding.ASCII.GetBytes(_password);

            using SHA1CryptoServiceProvider hashProvider = new SHA1CryptoServiceProvider();

            return hashProvider.ComputeHash(data);
        }

        public string EncryptPassword()
        {
            byte[] iv = new byte[16];

            byte[] array;

            Aes aes = Aes.Create();

            aes.Key = Encoding.UTF8.GetBytes(KEY);

            aes.IV = iv;

            ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

            using (MemoryStream memoryStream = new MemoryStream())
            {
                using CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write);
                using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
                {
                    streamWriter.Write(Password);
                }

                array = memoryStream.ToArray();
            }


            return Convert.ToBase64String(array);
        }

        private static string DecryptPassword(string cipherText)
        {
            byte[] iv = new byte[16];

            byte[] buffer = Convert.FromBase64String(cipherText);

            Aes aes = Aes.Create();

            aes.Key = Encoding.UTF8.GetBytes(KEY);

            aes.IV = iv;

            ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

            using MemoryStream memoryStream = new MemoryStream(buffer);

            using CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);

            using StreamReader streamReader = new StreamReader(cryptoStream);

            return streamReader.ReadToEnd();
        }

        private const string KEY = "b14ca5898a4e4133bbce2ea2315a1916";
        private string _userId;
        private string _password;
        private bool _remember;
    }
}
