﻿using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using ApiBase.Enums;
using Skeleton;

namespace ApplicationManager.Model.Users
{
    public class CreateUserModel : SkeletonModel
    {
        public Dictionary<ServiceInstitution, string> Institutions { get; set; }
        public ServiceInstitution Institution
        {
            get => _Institution;
            set
            {
                _Institution = value;
                switch (value)
                {
                    case ServiceInstitution.AlphaVantageApi:
                        HyperlinkUrl = "https://www.alphavantage.co/support/#api-key";
                        HyperlinkVisibility = Visibility.Visible;
                        break;
                    case ServiceInstitution.FinancialModelingPrepApi:
                        HyperlinkUrl = "https://financialmodelingprep.com/register";
                        HyperlinkVisibility = Visibility.Visible;
                        break;
                    default:
                        HyperlinkVisibility = Visibility.Hidden; 
                        break;
                }
                OnPropertyChanged();
            }
        }
        public string UserId
        {
            get => _userId;
            set
            {
                _userId = value;
                OnPropertyChanged();
            }
        }
        public string Password
        {
            get => _password;
            set
            {
                _password = value;
                OnPropertyChanged();
            }
        }
        public string RepeatPassword
        {
            get => _repeatPassword;
            set
            {
                _repeatPassword = value;
                OnPropertyChanged();
            }
        }
        public string ApiKey
        {
            get => _apiKey;
            set
            {
                _apiKey = value;
                OnPropertyChanged();
            }
        }
        public string HyperlinkUrl
        {
            get => _hyperlinkUrl;
            set
            {
                _hyperlinkUrl = value;
                OnPropertyChanged();
            }
        }
        public Visibility HyperlinkVisibility
        {
            get => _hyperlinkVisibility;
            set
            {
                _hyperlinkVisibility = value;
                OnPropertyChanged();
            }
        }

        public CreateUserModel()
        {
            Institutions = new Dictionary<ServiceInstitution, string>(1)
            {
                {
                    ServiceInstitution.AlphaVantageApi, "Alpha Vantage"
                },
                {
                    ServiceInstitution.FinancialModelingPrepApi, "Financial Modeling Prep"
                }
            };

            HyperlinkVisibility = Visibility.Hidden;
        }

        public byte[] GetPassword()
        {
            byte[] data = Encoding.ASCII.GetBytes(_password);

            using SHA1CryptoServiceProvider hashProvider = new SHA1CryptoServiceProvider();

            return hashProvider.ComputeHash(data);
        }

        private ServiceInstitution _Institution;
        private string _repeatPassword;
        private string _password;
        private string _userId;
        private string _apiKey;
        private string _hyperlinkUrl;
        private Visibility _hyperlinkVisibility;
    }
}
