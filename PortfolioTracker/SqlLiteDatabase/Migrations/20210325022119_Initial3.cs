﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DatabaseRepositories.Migrations
{
    public partial class Initial3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "UserSettings",
                keyColumn: "UserId",
                keyValue: "GM",
                column: "AutoUpdatePeriod",
                value: 60);

            migrationBuilder.UpdateData(
                table: "UserSettings",
                keyColumn: "UserId",
                keyValue: "SASA",
                column: "AutoUpdatePeriod",
                value: 60);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "UserSettings",
                keyColumn: "UserId",
                keyValue: "GM",
                column: "AutoUpdatePeriod",
                value: 0);

            migrationBuilder.UpdateData(
                table: "UserSettings",
                keyColumn: "UserId",
                keyValue: "SASA",
                column: "AutoUpdatePeriod",
                value: 0);
        }
    }
}
