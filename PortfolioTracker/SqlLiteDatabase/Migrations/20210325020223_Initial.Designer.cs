﻿// <auto-generated />
using System;
using DatabaseRepositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace DatabaseRepositories.Migrations
{
    [DbContext(typeof(SqlLiteDatabaseContext))]
    [Migration("20210325020223_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "5.0.4");

            modelBuilder.Entity("DatabaseModels.Stocks.CompanyOverview", b =>
                {
                    b.Property<string>("Symbol")
                        .HasColumnType("TEXT");

                    b.Property<string>("AssetType")
                        .HasColumnType("TEXT");

                    b.Property<string>("BookValue")
                        .HasColumnType("TEXT");

                    b.Property<string>("Country")
                        .HasColumnType("TEXT");

                    b.Property<string>("Currency")
                        .HasColumnType("TEXT");

                    b.Property<string>("Description")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("DividendDate")
                        .HasColumnType("TEXT");

                    b.Property<decimal>("DividendPerShare")
                        .HasColumnType("TEXT");

                    b.Property<decimal>("DividendYield")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("ExDividendDate")
                        .HasColumnType("TEXT");

                    b.Property<string>("Exchange")
                        .HasColumnType("TEXT");

                    b.Property<string>("FiftyDayMovingAverage")
                        .HasColumnType("TEXT");

                    b.Property<string>("FiscalYearEnd")
                        .HasColumnType("TEXT");

                    b.Property<string>("Industry")
                        .HasColumnType("TEXT");

                    b.Property<DateTime?>("LastUpdate")
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.Property<string>("OperatingMarginTTM")
                        .HasColumnType("TEXT");

                    b.Property<string>("ProfitMargin")
                        .HasColumnType("TEXT");

                    b.Property<string>("RevenuePerShareTTM")
                        .HasColumnType("TEXT");

                    b.Property<string>("RevenueTTM")
                        .HasColumnType("TEXT");

                    b.Property<string>("Sector")
                        .HasColumnType("TEXT");

                    b.Property<string>("TwoHundredDayMovingAverage")
                        .HasColumnType("TEXT");

                    b.HasKey("Symbol");

                    b.ToTable("CompanyOverviews");
                });

            modelBuilder.Entity("DatabaseModels.Users.ServiceConfig", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnType("TEXT");

                    b.Property<int>("OverviewService")
                        .HasColumnType("INTEGER");

                    b.Property<int>("QuoteService")
                        .HasColumnType("INTEGER");

                    b.Property<int>("SearchService")
                        .HasColumnType("INTEGER");

                    b.HasKey("UserId");

                    b.ToTable("ServiceConfigs");

                    b.HasData(
                        new
                        {
                            UserId = "SASA",
                            OverviewService = 1,
                            QuoteService = 1,
                            SearchService = 1
                        },
                        new
                        {
                            UserId = "GM",
                            OverviewService = 1,
                            QuoteService = 1,
                            SearchService = 1
                        });
                });

            modelBuilder.Entity("DatabaseModels.Users.ServiceKey", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("ApiKey")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<int>("ServiceTypeId")
                        .HasColumnType("INTEGER");

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.HasIndex("ServiceTypeId");

                    b.HasIndex("UserId");

                    b.ToTable("ServiceKeys");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            ApiKey = "G4O0655M4EUOLEXI",
                            ServiceTypeId = 1,
                            UserId = "SASA"
                        },
                        new
                        {
                            Id = 2,
                            ApiKey = "WFNCFQYEMNMXLZF8",
                            ServiceTypeId = 1,
                            UserId = "GM"
                        });
                });

            modelBuilder.Entity("DatabaseModels.Users.ServiceType", b =>
                {
                    b.Property<int>("Id")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("ServiceTypes");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Name = "Alpha Vantage Service"
                        });
                });

            modelBuilder.Entity("DatabaseModels.Users.StockTrade", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<bool>("Buy")
                        .HasColumnType("INTEGER");

                    b.Property<DateTime?>("Date")
                        .HasColumnType("TEXT");

                    b.Property<decimal>("Price")
                        .HasColumnType("TEXT");

                    b.Property<decimal>("Quantity")
                        .HasColumnType("TEXT");

                    b.Property<int>("UserStockId")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.HasIndex("UserStockId");

                    b.ToTable("StockTrades");
                });

            modelBuilder.Entity("DatabaseModels.Users.User", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnType("TEXT");

                    b.Property<byte[]>("Password")
                        .IsRequired()
                        .HasMaxLength(20)
                        .HasColumnType("BLOB");

                    b.HasKey("UserId");

                    b.HasIndex("UserId")
                        .IsUnique();

                    b.ToTable("Users");

                    b.HasData(
                        new
                        {
                            UserId = "SASA",
                            Password = new byte[] { 40, 145, 186, 206, 238, 241, 101, 46, 230, 152, 41, 77, 160, 231, 27, 167, 138, 42, 64, 100 }
                        },
                        new
                        {
                            UserId = "GM",
                            Password = new byte[] { 46, 140, 2, 119, 227, 150, 250, 191, 104, 62, 86, 200, 183, 250, 126, 109, 173, 104, 198, 121 }
                        });
                });

            modelBuilder.Entity("DatabaseModels.Users.UserSetting", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnType("TEXT");

                    b.Property<bool>("AutoUpdate")
                        .HasColumnType("INTEGER");

                    b.Property<int>("AutoUpdatePeriod")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("OnStartUpdate")
                        .HasColumnType("INTEGER");

                    b.HasKey("UserId");

                    b.ToTable("UserSettings");
                });

            modelBuilder.Entity("DatabaseModels.Users.UserStock", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<decimal>("AveragePrice")
                        .HasColumnType("TEXT");

                    b.Property<bool>("HasDividends")
                        .HasColumnType("INTEGER");

                    b.Property<DateTime?>("LastUpdate")
                        .HasColumnType("TEXT");

                    b.Property<string>("Symbol")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<decimal>("TotalQuantity")
                        .HasColumnType("TEXT");

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.HasIndex("Symbol");

                    b.HasIndex("UserId");

                    b.ToTable("UserStocks");
                });

            modelBuilder.Entity("DatabaseModels.Users.ServiceConfig", b =>
                {
                    b.HasOne("DatabaseModels.Users.User", "User")
                        .WithOne("ServiceConfig")
                        .HasForeignKey("DatabaseModels.Users.ServiceConfig", "UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("User");
                });

            modelBuilder.Entity("DatabaseModels.Users.ServiceKey", b =>
                {
                    b.HasOne("DatabaseModels.Users.ServiceType", "ServiceType")
                        .WithMany()
                        .HasForeignKey("ServiceTypeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DatabaseModels.Users.User", "User")
                        .WithMany("ServiceKeys")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("ServiceType");

                    b.Navigation("User");
                });

            modelBuilder.Entity("DatabaseModels.Users.StockTrade", b =>
                {
                    b.HasOne("DatabaseModels.Users.UserStock", "UserStock")
                        .WithMany("Trades")
                        .HasForeignKey("UserStockId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("UserStock");
                });

            modelBuilder.Entity("DatabaseModels.Users.UserSetting", b =>
                {
                    b.HasOne("DatabaseModels.Users.User", "User")
                        .WithOne("UserSetting")
                        .HasForeignKey("DatabaseModels.Users.UserSetting", "UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("User");
                });

            modelBuilder.Entity("DatabaseModels.Users.UserStock", b =>
                {
                    b.HasOne("DatabaseModels.Stocks.CompanyOverview", "CompanyOverview")
                        .WithMany()
                        .HasForeignKey("Symbol")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DatabaseModels.Users.User", "User")
                        .WithMany("PortfolioStocks")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("CompanyOverview");

                    b.Navigation("User");
                });

            modelBuilder.Entity("DatabaseModels.Users.User", b =>
                {
                    b.Navigation("PortfolioStocks");

                    b.Navigation("ServiceConfig");

                    b.Navigation("ServiceKeys");

                    b.Navigation("UserSetting");
                });

            modelBuilder.Entity("DatabaseModels.Users.UserStock", b =>
                {
                    b.Navigation("Trades");
                });
#pragma warning restore 612, 618
        }
    }
}
