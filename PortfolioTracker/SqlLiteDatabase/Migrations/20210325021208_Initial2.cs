﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DatabaseRepositories.Migrations
{
    public partial class Initial2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "UserSettings",
                columns: new[] { "UserId", "AutoUpdate", "AutoUpdatePeriod", "OnStartUpdate" },
                values: new object[] { "SASA", false, 0, false });

            migrationBuilder.InsertData(
                table: "UserSettings",
                columns: new[] { "UserId", "AutoUpdate", "AutoUpdatePeriod", "OnStartUpdate" },
                values: new object[] { "GM", false, 0, false });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "UserSettings",
                keyColumn: "UserId",
                keyValue: "GM");

            migrationBuilder.DeleteData(
                table: "UserSettings",
                keyColumn: "UserId",
                keyValue: "SASA");
        }
    }
}
