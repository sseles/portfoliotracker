﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DatabaseRepositories.Migrations
{
    public partial class FMPAddedToSeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "ServiceTypes",
                columns: new[] { "Id", "Name" },
                values: new object[] { 2, "Financial Modeling Prep" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ServiceTypes",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
