﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DatabaseRepositories.Migrations
{
    public partial class ServiceKeyNameRequired : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ServiceKeys",
                type: "TEXT",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "ServiceKeys",
                keyColumn: "Id",
                keyValue: 1,
                column: "Name",
                value: "Alpha_1");

            migrationBuilder.UpdateData(
                table: "ServiceKeys",
                keyColumn: "Id",
                keyValue: 2,
                column: "Name",
                value: "Alpha_1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ServiceKeys",
                type: "TEXT",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "TEXT");

            migrationBuilder.UpdateData(
                table: "ServiceKeys",
                keyColumn: "Id",
                keyValue: 1,
                column: "Name",
                value: null);

            migrationBuilder.UpdateData(
                table: "ServiceKeys",
                keyColumn: "Id",
                keyValue: 2,
                column: "Name",
                value: null);
        }
    }
}
