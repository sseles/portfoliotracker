﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DatabaseRepositories.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CompanyOverviews",
                columns: table => new
                {
                    Symbol = table.Column<string>(type: "TEXT", nullable: false),
                    AssetType = table.Column<string>(type: "TEXT", nullable: true),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    Exchange = table.Column<string>(type: "TEXT", nullable: true),
                    Currency = table.Column<string>(type: "TEXT", nullable: true),
                    Country = table.Column<string>(type: "TEXT", nullable: true),
                    Industry = table.Column<string>(type: "TEXT", nullable: true),
                    Sector = table.Column<string>(type: "TEXT", nullable: true),
                    Description = table.Column<string>(type: "TEXT", nullable: true),
                    BookValue = table.Column<string>(type: "TEXT", nullable: true),
                    DividendPerShare = table.Column<decimal>(type: "TEXT", nullable: false),
                    DividendYield = table.Column<decimal>(type: "TEXT", nullable: false),
                    ExDividendDate = table.Column<DateTime>(type: "TEXT", nullable: false),
                    DividendDate = table.Column<DateTime>(type: "TEXT", nullable: false),
                    FiscalYearEnd = table.Column<string>(type: "TEXT", nullable: true),
                    RevenuePerShareTTM = table.Column<string>(type: "TEXT", nullable: true),
                    ProfitMargin = table.Column<string>(type: "TEXT", nullable: true),
                    RevenueTTM = table.Column<string>(type: "TEXT", nullable: true),
                    OperatingMarginTTM = table.Column<string>(type: "TEXT", nullable: true),
                    FiftyDayMovingAverage = table.Column<string>(type: "TEXT", nullable: true),
                    TwoHundredDayMovingAverage = table.Column<string>(type: "TEXT", nullable: true),
                    LastUpdate = table.Column<DateTime>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyOverviews", x => x.Symbol);
                });

            migrationBuilder.CreateTable(
                name: "ServiceTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false),
                    Name = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "TEXT", nullable: false),
                    Password = table.Column<byte[]>(type: "BLOB", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "ServiceConfigs",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "TEXT", nullable: false),
                    SearchService = table.Column<int>(type: "INTEGER", nullable: false),
                    QuoteService = table.Column<int>(type: "INTEGER", nullable: false),
                    OverviewService = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceConfigs", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_ServiceConfigs_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ServiceKeys",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserId = table.Column<string>(type: "TEXT", nullable: false),
                    ApiKey = table.Column<string>(type: "TEXT", nullable: false),
                    ServiceTypeId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceKeys", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ServiceKeys_ServiceTypes_ServiceTypeId",
                        column: x => x.ServiceTypeId,
                        principalTable: "ServiceTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ServiceKeys_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserSettings",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "TEXT", nullable: false),
                    OnStartUpdate = table.Column<bool>(type: "INTEGER", nullable: false),
                    AutoUpdate = table.Column<bool>(type: "INTEGER", nullable: false),
                    AutoUpdatePeriod = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSettings", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_UserSettings_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserStocks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserId = table.Column<string>(type: "TEXT", nullable: false),
                    Symbol = table.Column<string>(type: "TEXT", nullable: false),
                    TotalQuantity = table.Column<decimal>(type: "TEXT", nullable: false),
                    AveragePrice = table.Column<decimal>(type: "TEXT", nullable: false),
                    HasDividends = table.Column<bool>(type: "INTEGER", nullable: false),
                    LastUpdate = table.Column<DateTime>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserStocks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserStocks_CompanyOverviews_Symbol",
                        column: x => x.Symbol,
                        principalTable: "CompanyOverviews",
                        principalColumn: "Symbol",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserStocks_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StockTrades",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserStockId = table.Column<int>(type: "INTEGER", nullable: false),
                    Quantity = table.Column<decimal>(type: "TEXT", nullable: false),
                    Price = table.Column<decimal>(type: "TEXT", nullable: false),
                    Buy = table.Column<bool>(type: "INTEGER", nullable: false),
                    Date = table.Column<DateTime>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StockTrades", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StockTrades_UserStocks_UserStockId",
                        column: x => x.UserStockId,
                        principalTable: "UserStocks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "ServiceTypes",
                columns: new[] { "Id", "Name" },
                values: new object[] { 1, "Alpha Vantage Service" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserId", "Password" },
                values: new object[] { "SASA", new byte[] { 40, 145, 186, 206, 238, 241, 101, 46, 230, 152, 41, 77, 160, 231, 27, 167, 138, 42, 64, 100 } });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserId", "Password" },
                values: new object[] { "GM", new byte[] { 46, 140, 2, 119, 227, 150, 250, 191, 104, 62, 86, 200, 183, 250, 126, 109, 173, 104, 198, 121 } });

            migrationBuilder.InsertData(
                table: "ServiceConfigs",
                columns: new[] { "UserId", "OverviewService", "QuoteService", "SearchService" },
                values: new object[] { "SASA", 1, 1, 1 });

            migrationBuilder.InsertData(
                table: "ServiceConfigs",
                columns: new[] { "UserId", "OverviewService", "QuoteService", "SearchService" },
                values: new object[] { "GM", 1, 1, 1 });

            migrationBuilder.InsertData(
                table: "ServiceKeys",
                columns: new[] { "Id", "ApiKey", "ServiceTypeId", "UserId" },
                values: new object[] { 1, "G4O0655M4EUOLEXI", 1, "SASA" });

            migrationBuilder.InsertData(
                table: "ServiceKeys",
                columns: new[] { "Id", "ApiKey", "ServiceTypeId", "UserId" },
                values: new object[] { 2, "WFNCFQYEMNMXLZF8", 1, "GM" });

            migrationBuilder.CreateIndex(
                name: "IX_ServiceKeys_ServiceTypeId",
                table: "ServiceKeys",
                column: "ServiceTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceKeys_UserId",
                table: "ServiceKeys",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_StockTrades_UserStockId",
                table: "StockTrades",
                column: "UserStockId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserId",
                table: "Users",
                column: "UserId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserStocks_Symbol",
                table: "UserStocks",
                column: "Symbol");

            migrationBuilder.CreateIndex(
                name: "IX_UserStocks_UserId",
                table: "UserStocks",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ServiceConfigs");

            migrationBuilder.DropTable(
                name: "ServiceKeys");

            migrationBuilder.DropTable(
                name: "StockTrades");

            migrationBuilder.DropTable(
                name: "UserSettings");

            migrationBuilder.DropTable(
                name: "ServiceTypes");

            migrationBuilder.DropTable(
                name: "UserStocks");

            migrationBuilder.DropTable(
                name: "CompanyOverviews");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
