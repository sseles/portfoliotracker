﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DatabaseRepositories.Migrations
{
    public partial class CompanyOverviewSimplified : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AssetType",
                table: "CompanyOverviews");

            migrationBuilder.DropColumn(
                name: "BookValue",
                table: "CompanyOverviews");

            migrationBuilder.DropColumn(
                name: "DividendDate",
                table: "CompanyOverviews");

            migrationBuilder.DropColumn(
                name: "DividendPerShare",
                table: "CompanyOverviews");

            migrationBuilder.DropColumn(
                name: "DividendYield",
                table: "CompanyOverviews");

            migrationBuilder.DropColumn(
                name: "ExDividendDate",
                table: "CompanyOverviews");

            migrationBuilder.DropColumn(
                name: "FiftyDayMovingAverage",
                table: "CompanyOverviews");

            migrationBuilder.DropColumn(
                name: "FiscalYearEnd",
                table: "CompanyOverviews");

            migrationBuilder.DropColumn(
                name: "OperatingMarginTTM",
                table: "CompanyOverviews");

            migrationBuilder.DropColumn(
                name: "ProfitMargin",
                table: "CompanyOverviews");

            migrationBuilder.DropColumn(
                name: "RevenuePerShareTTM",
                table: "CompanyOverviews");

            migrationBuilder.DropColumn(
                name: "RevenueTTM",
                table: "CompanyOverviews");

            migrationBuilder.DropColumn(
                name: "TwoHundredDayMovingAverage",
                table: "CompanyOverviews");

            migrationBuilder.AddColumn<bool>(
                name: "HasDividends",
                table: "CompanyOverviews",
                type: "INTEGER",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HasDividends",
                table: "CompanyOverviews");

            migrationBuilder.AddColumn<string>(
                name: "AssetType",
                table: "CompanyOverviews",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BookValue",
                table: "CompanyOverviews",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DividendDate",
                table: "CompanyOverviews",
                type: "TEXT",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<decimal>(
                name: "DividendPerShare",
                table: "CompanyOverviews",
                type: "TEXT",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "DividendYield",
                table: "CompanyOverviews",
                type: "TEXT",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<DateTime>(
                name: "ExDividendDate",
                table: "CompanyOverviews",
                type: "TEXT",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FiftyDayMovingAverage",
                table: "CompanyOverviews",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FiscalYearEnd",
                table: "CompanyOverviews",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OperatingMarginTTM",
                table: "CompanyOverviews",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProfitMargin",
                table: "CompanyOverviews",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RevenuePerShareTTM",
                table: "CompanyOverviews",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RevenueTTM",
                table: "CompanyOverviews",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TwoHundredDayMovingAverage",
                table: "CompanyOverviews",
                type: "TEXT",
                nullable: true);
        }
    }
}
