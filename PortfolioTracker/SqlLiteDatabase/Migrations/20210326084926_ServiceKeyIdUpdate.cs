﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DatabaseRepositories.Migrations
{
    public partial class ServiceKeyIdUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SearchService",
                table: "ServiceConfigs",
                newName: "SearchServiceKey");

            migrationBuilder.RenameColumn(
                name: "QuoteService",
                table: "ServiceConfigs",
                newName: "QuoteServiceKey");

            migrationBuilder.RenameColumn(
                name: "OverviewService",
                table: "ServiceConfigs",
                newName: "OverviewServiceKey");

            migrationBuilder.UpdateData(
                table: "ServiceConfigs",
                keyColumn: "UserId",
                keyValue: "GM",
                columns: new[] { "OverviewServiceKey", "QuoteServiceKey", "SearchServiceKey" },
                values: new object[] { 2, 2, 2 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SearchServiceKey",
                table: "ServiceConfigs",
                newName: "SearchService");

            migrationBuilder.RenameColumn(
                name: "QuoteServiceKey",
                table: "ServiceConfigs",
                newName: "QuoteService");

            migrationBuilder.RenameColumn(
                name: "OverviewServiceKey",
                table: "ServiceConfigs",
                newName: "OverviewService");

            migrationBuilder.UpdateData(
                table: "ServiceConfigs",
                keyColumn: "UserId",
                keyValue: "GM",
                columns: new[] { "OverviewService", "QuoteService", "SearchService" },
                values: new object[] { 1, 1, 1 });
        }
    }
}
