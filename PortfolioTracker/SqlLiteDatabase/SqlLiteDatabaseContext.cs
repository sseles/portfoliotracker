﻿using System.Data.SQLite;
using DatabaseModels.Stocks;
using DatabaseModels.Users;
using DatabaseRepositories.Properties;
using Microsoft.EntityFrameworkCore;

namespace DatabaseRepositories
{
    public class SqlLiteDatabaseContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<ServiceType> ServiceTypes { get; set; }
        public DbSet<ServiceConfig> ServiceConfigs { get; set; }
        public DbSet<UserSetting> UserSettings { get; set; }
        public DbSet<UserStock> UserStocks { get; set; }
        public DbSet<CompanyOverview> CompanyOverviews { get; set; }
        public DbSet<StockTrade> StockTrades { get; set; }
        public DbSet<ServiceKey> ServiceKeys { get; set; }

        public SqlLiteDatabaseContext() : base()
        {
        }

        public SqlLiteDatabaseContext(DbContextOptions<SqlLiteDatabaseContext> options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            SQLiteConnection connection = new SQLiteConnection(Resources.ConnectionString);

            connection.Open();

            SQLiteCommand command = connection.CreateCommand();

            command.CommandText = string.Format("PRAGMA key = {0};", Resources.DatabasePassword);

            command.ExecuteNonQuery();

            optionsBuilder.UseSqlite(connection);

            optionsBuilder.EnableSensitiveDataLogging();

            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasIndex(p => p.UserId)
                .IsUnique(true);

            modelBuilder.Entity<User>()
                .HasMany<ServiceKey>(p => p.ServiceKeys);

            modelBuilder.Entity<User>()
                .HasOne<ServiceConfig>(p => p.ServiceConfig);

            modelBuilder.Entity<User>()
                .HasOne<UserSetting>(p => p.UserSetting);

            modelBuilder.Entity<User>()
                .HasMany<UserStock>(p => p.PortfolioStocks);

            modelBuilder.Entity<UserStock>()
                .HasMany<StockTrade>(p => p.Trades);

            modelBuilder.Entity<ServiceType>()
                .HasData(DatabaseSeed.CreateApiServices());

#if DEBUG
            modelBuilder.Entity<User>()
                .HasData(DatabaseSeed.CreateUsers());

            modelBuilder.Entity<ServiceKey>()
                .HasData(DatabaseSeed.CreateServiceKeys());

            modelBuilder.Entity<ServiceConfig>()
                .HasData(DatabaseSeed.CreateServiceConfigs());

            modelBuilder.Entity<UserSetting>()
                .HasData(DatabaseSeed.CreateUserSettings());
#endif

            base.OnModelCreating(modelBuilder);
        }
    }
}
//Remove-Migration
//Add-Migration Database
//Update-Database 