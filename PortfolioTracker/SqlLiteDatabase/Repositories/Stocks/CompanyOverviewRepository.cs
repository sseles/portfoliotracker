﻿using DatabaseModels.Stocks;
using DatabaseRepositories.Repositories.Abstracts;
using Microsoft.EntityFrameworkCore;

namespace DatabaseRepositories.Repositories.Stocks
{
    public class CompanyOverviewRepository : BaseRepository<CompanyOverview>
    {
        public CompanyOverviewRepository(DbContext databaseContext) : base(databaseContext)
        {
        }
    }
}
