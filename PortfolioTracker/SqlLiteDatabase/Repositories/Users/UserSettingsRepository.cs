﻿using DatabaseModels.Users;
using DatabaseRepositories.Repositories.Abstracts;
using Microsoft.EntityFrameworkCore;

namespace SqlLiteDatabase.Repositories.Users
{
    public class UserSettingsRepository : BaseRepository<UserSetting>
    {
        public UserSettingsRepository(DbContext databaseContext) : base(databaseContext)
        {
        }
    }
}
