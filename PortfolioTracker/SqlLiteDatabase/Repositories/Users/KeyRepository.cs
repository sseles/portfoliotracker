﻿using DatabaseModels.Users;
using DatabaseRepositories.Repositories.Abstracts;
using Microsoft.EntityFrameworkCore;

namespace SqlLiteDatabase.Repositories.Users
{
    public class KeyRepository : BaseRepository<ServiceKey>
    {
        public KeyRepository(DbContext databaseContext) : base(databaseContext)
        {
        }
    }
}
