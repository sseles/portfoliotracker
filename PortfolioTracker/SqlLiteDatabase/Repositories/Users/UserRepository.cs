﻿using System.Threading.Tasks;
using DatabaseModels.Users;
using DatabaseRepositories.Repositories.Abstracts;
using Microsoft.EntityFrameworkCore;

namespace SqlLiteDatabase.Repositories.Users
{
    public class UserRepository : BaseRepository<User>
    {
        public UserRepository(DbContext databaseContext) : base(databaseContext)
        {
        }

        public override Task<User> GetById(object id)
        {
            return _dbSet
                .Include(p => p.ServiceKeys)
                .Include(p => p.PortfolioStocks)
                .ThenInclude(p => p.Trades)
                .Include(p => p.PortfolioStocks)
                .ThenInclude(p => p.CompanyOverview)
                .Include(p => p.ServiceConfig)
                .Include(p => p.UserSetting)
                .SingleOrDefaultAsync(p => p.UserId == (string)id);
        }
    }
}
