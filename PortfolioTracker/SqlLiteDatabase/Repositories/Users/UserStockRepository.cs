﻿using DatabaseModels.Users;
using DatabaseRepositories.Repositories.Abstracts;
using Microsoft.EntityFrameworkCore;

namespace SqlLiteDatabase.Repositories.Users
{
    public class UserStockRepository : BaseRepository<UserStock>
    {
        public UserStockRepository(DbContext databaseContext) : base(databaseContext)
        {
        }
    }
}
