﻿using DatabaseModels.Users;
using DatabaseRepositories.Repositories.Abstracts;
using Microsoft.EntityFrameworkCore;

namespace SqlLiteDatabase.Repositories.Users
{
    public class ServiceConfigRepository : BaseRepository<ServiceConfig>
    {
        public ServiceConfigRepository(DbContext databaseContext) : base(databaseContext)
        {
        }
    }
}
