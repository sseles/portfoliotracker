﻿using DatabaseModels.Users;
using DatabaseRepositories.Repositories.Abstracts;
using Microsoft.EntityFrameworkCore;

namespace DatabaseRepositories.Repositories.Users
{
    public class StockQuantityRepository : BaseRepository<StockTrade>
    {
        public StockQuantityRepository(DbContext databaseContext) : base(databaseContext)
        {
        }
    }
}
