﻿using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using ApiBase.Enums;
using DatabaseModels.Users;

namespace DatabaseRepositories
{
    public static class DatabaseSeed
    {
        static readonly Dictionary<string, User> _users;
        static readonly Dictionary<string, ServiceType> _services;

        static DatabaseSeed()
        {
            #region Users
            _users = new Dictionary<string, User>(2);

            User sasa = new User()
            {
                UserId = "SASA",
                Password = GetPassword("asd123")
            };

            User amine = new User()
            {
                UserId = "GM",
                Password = GetPassword("2002")
            };
           
            _users.Add("sasa", sasa);

            _users.Add("amine", amine);
            #endregion

            #region Services
            _services = new Dictionary<string, ServiceType>();

            ServiceType fmpService = new ServiceType()
            {
                Id = ServiceInstitution.FinancialModelingPrepApi,
                Name = "Financial Modeling Prep"
            };

            _services.Add("fmp", fmpService);

            ServiceType alphaService = new ServiceType()
            {
                Id = ServiceInstitution.AlphaVantageApi,
                Name = "Alpha Vantage Service"
            };

            _services.Add("alpha", alphaService);
            #endregion
        }

        public static IEnumerable<User> CreateUsers()
        {
            return _users.Values;
        }

        public static IEnumerable<ServiceKey> CreateServiceKeys()
        {
            List<ServiceKey> keys = new List<ServiceKey>
            {
                new ServiceKey()
                {
                    Id = 1,
                    Name = "Alpha_1",
                    ServiceTypeId = ServiceInstitution.AlphaVantageApi,
                    UserId = _users["sasa"].UserId,
                    ApiKey = "G4O0655M4EUOLEXI"
                },
                new ServiceKey()
                {
                    Id = 2,
                    Name = "Alpha_1",
                    ServiceTypeId = ServiceInstitution.AlphaVantageApi,
                    UserId = _users["amine"].UserId,
                    ApiKey = "WFNCFQYEMNMXLZF8"
                }
            };

            return keys;
        }

        public static IEnumerable<ServiceConfig> CreateServiceConfigs()
        {
            List<ServiceConfig> configs = new List<ServiceConfig>
            {
                new ServiceConfig()
                {
                    UserId = _users["sasa"].UserId,
                    QuoteServiceKey =  1,
                    SearchServiceKey = 1,
                    OverviewServiceKey = 1,
                },
                new ServiceConfig()
                {
                    UserId = _users["amine"].UserId,
                    QuoteServiceKey =  2,
                    SearchServiceKey = 2,
                    OverviewServiceKey = 2,
                }
            };

            return configs;
        }

        public static IEnumerable<ServiceType> CreateApiServices()
        {
            return _services.Values;
        }

        public static IEnumerable<UserSetting> CreateUserSettings()
        {
            List<UserSetting> settings = new List<UserSetting>();

            UserSetting sasa = new UserSetting()
            {
                UserId = _users["sasa"].UserId,
                AutoUpdatePeriod = 60
            };

            UserSetting amine = new UserSetting()
            {
                UserId = _users["amine"].UserId,
                AutoUpdatePeriod = 60
            };

            settings.Add(sasa);

            settings.Add(amine);

            return settings;
        }

        private static byte[] GetPassword(string password)
        {
            byte[] data = Encoding.ASCII.GetBytes(password);

            using SHA1CryptoServiceProvider hashProvider = new SHA1CryptoServiceProvider();

            return hashProvider.ComputeHash(data);
        }
    }
}
