﻿using System.ComponentModel.DataAnnotations;
using ApiBase.Interfaces.Models;

namespace DatabaseModels.Users
{
    public class ServiceConfig: IServiceConfig
    {
        [Key]
        [Required]
        public string UserId { get; set; }
        [Required]
        public int SearchServiceKey { get; set; }
        [Required]
        public int QuoteServiceKey { get; set; }
        [Required]
        public int OverviewServiceKey { get; set; }
        public User User { get; set; }
    }
}
