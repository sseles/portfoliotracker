﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DatabaseModels.Base;

namespace DatabaseModels.Users
{
    public class User : BaseModel
    {
        [Key]
        [Required]
        public string UserId { get; set; }
        [Required]
        [MinLength(20)]
        [MaxLength(20)]
        public byte[] Password { get; set; }

        public User()
        {
            PortfolioStocks = new List<UserStock>();

            ServiceKeys = new List<ServiceKey>();
        }

        public ICollection<UserStock> PortfolioStocks { get; set; }
        public ICollection<ServiceKey> ServiceKeys { get; set; }
        public ServiceConfig ServiceConfig { get; set; }
        public UserSetting UserSetting { get; set; }
    }
}
