﻿using ApiBase.Enums;

namespace DatabaseModels.Users
{
    public class ServiceType
    {
        public ServiceInstitution Id { get; set; }
        public string Name { get; set; }
    }
}
