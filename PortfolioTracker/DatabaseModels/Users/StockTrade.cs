﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DatabaseModels.Users
{
    public class StockTrade
    {
        [Key]
        [Required]
        public int Id { get; set; }
        [Required]
        public int UserStockId { get; set; }
        [Required]
        public decimal Quantity { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        public bool Buy { get; set; }
        public DateTime? Date { get; set; }
        public UserStock UserStock { get; set; }
    }
}
