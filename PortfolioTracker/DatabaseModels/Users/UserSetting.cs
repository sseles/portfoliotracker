﻿using System.ComponentModel.DataAnnotations;

namespace DatabaseModels.Users
{
    public class UserSetting
    {
        [Key]
        [Required]
        public string UserId { get; set; }
        public bool OnStartUpdate { get; set; }
        public bool AutoUpdate { get; set; }
        public int AutoUpdatePeriod { get; set; }
        public User User { get; set; }
    }
}
