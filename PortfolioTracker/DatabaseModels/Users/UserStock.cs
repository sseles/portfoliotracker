﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DatabaseModels.Base;
using DatabaseModels.Stocks;

namespace DatabaseModels.Users
{
    public class UserStock : BaseModel
    {
        [Key]
        [Required]
        public int Id { get; set; }
        [Required]
        public string UserId { get; set; }
        [Required]
        [ForeignKey("CompanyOverview")]
        public string Symbol { get; set; }
        [Required]
        public decimal TotalQuantity { get; set; }
        [Required]
        public decimal AveragePrice { get; set; }
        [Required]
        public bool HasDividends { get; set; }
        public DateTime? LastUpdate { get; set; }
        public User User { get; set; }
        public CompanyOverview CompanyOverview { get; set; }
        public ICollection<StockTrade> Trades { get; set; }

        public UserStock()
        {
            Trades = new List<StockTrade>();
        }
    }
}
