﻿using System.ComponentModel.DataAnnotations;
using ApiBase.Enums;
using DatabaseModels.Base;

namespace DatabaseModels.Users
{
    public class ServiceKey : BaseModel
    {
        [Key]
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string UserId { get; set; }
        [Required]
        public string ApiKey { get; set; }
        [Required]
        public ServiceInstitution ServiceTypeId { get; set; }
        public User User { get; set; }
        public ServiceType ServiceType { get; set; }
    }
}
