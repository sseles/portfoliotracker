﻿using System;
using System.ComponentModel.DataAnnotations;
using DatabaseModels.Base;
using ApiBase.Interfaces.Models;

namespace DatabaseModels.Stocks
{
    public class CompanyOverview : BaseModel, ICompanyOverview
    {
        [Key]
        [Required]
        public string Symbol { get; set; }
        public string Name { get; set; }
        public string Exchange { get; set; }
        public string Currency { get; set; }
        public string Country { get; set; }
        public string Industry { get; set; }
        public string Sector { get; set; }
        public string Description { get; set; }
        public bool HasDividends { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}
