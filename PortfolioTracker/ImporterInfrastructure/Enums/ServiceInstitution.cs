﻿namespace ApiBase.Enums
{
    public enum ServiceInstitution
    {
        AlphaVantageApi = 1,
        FinancialModelingPrepApi = 2
    }
}
