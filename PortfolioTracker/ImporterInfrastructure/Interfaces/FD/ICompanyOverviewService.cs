﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ApiBase.Interfaces.Models;

namespace ApiBase.Interfaces.FD
{
    public interface ICompanyOverviewService
    {
        Task<ICompanyOverview> GetCompanyOverview(string symbol, CancellationToken? token);
        Task<IEnumerable<ICompanyOverview>> GetCompanyOverviews(params string[] symbols);
        Task<IEnumerable<ICompanyOverview>> GetCompanyOverviews(CancellationToken? token, params string[] symbols);
    }
}
