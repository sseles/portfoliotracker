﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ApiBase.Interfaces.Models;

namespace ApiBase.Interfaces.FD
{
    public interface IQuoteService
    {
        Task<ICompanyQuote> GetQuote(string symbol, CancellationToken? token);
        Task<IEnumerable<ICompanyQuote>> GetQuotes(params string[] symbols);
        Task<IEnumerable<ICompanyQuote>> GetQuotes(CancellationToken? token, params string[] symbols);
    }
}
