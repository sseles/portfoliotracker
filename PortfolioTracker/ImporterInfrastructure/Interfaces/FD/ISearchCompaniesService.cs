﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ApiBase.Interfaces.Models;

namespace ApiBase.Interfaces.FD
{
    public interface ISearchCompaniesService
    {
        Task<IEnumerable<ISymbolSearchResult>> SearchCompanies(string keywords, CancellationToken? token);
    }
}
