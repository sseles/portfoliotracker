﻿using System;

namespace ApiBase.Interfaces.Models
{
    public interface ICompanyQuote
    {
        string Symbol { get; set; }
        decimal Open { get; set; }
        decimal High { get; set; }
        decimal Low { get; set; }
        decimal Price { get; set; }
        uint Volume { get; set; }
        decimal PreviousClose { get; set; }
        string ChangePercent { get; set; }
    }
}
