﻿namespace ApiBase.Interfaces.Models
{
    public interface IServiceConfig
    {
        int SearchServiceKey { get; set; }
        int QuoteServiceKey { get; set; }
        int OverviewServiceKey { get; set; }
    }
}
