﻿using System;

namespace ApiBase.Interfaces.Models
{
    public interface ISymbolSearchResult
    {
        string Symbol { get; set; }
        string Name { get; set; }
        string Type { get; set; }
        string Region { get; set; }
        string Currency { get; set; }
        decimal MatchScore { get; set; }
    }
}
