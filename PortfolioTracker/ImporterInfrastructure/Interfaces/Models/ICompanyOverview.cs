﻿using System;

namespace ApiBase.Interfaces.Models
{
    public interface ICompanyOverview
    {
        string Symbol { get; set; }
        string Name { get; set; }
        string Exchange { get; set; }
        string Currency { get; set; }
        string Country { get; set; }
        string Industry { get; set; }
        string Sector { get; set; }
        string Description { get; set; }
        bool HasDividends { get; set; }
        DateTime? LastUpdate { get; set; }
    }
}
