﻿using System;
using ApiBase.Interfaces.FD;

namespace ApiBase.Interfaces
{
    public interface IStockService : IDisposable
    {
        public ISearchCompaniesService SearchCompaniesService { get; }
        public IQuoteService QuoteService { get; }
        public ICompanyOverviewService CompanyOverviewService { get; }
    }
}
