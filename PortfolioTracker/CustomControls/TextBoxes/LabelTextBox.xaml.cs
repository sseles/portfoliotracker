﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace CustomControls.TextBoxes
{
    public partial class LabelTextBox : UserControl
    {

        public static readonly DependencyProperty LabelProperty =
  DependencyProperty.RegisterAttached(nameof(LabelText), typeof(object), typeof(LabelTextBox),
      new FrameworkPropertyMetadata(default(string), Label_OnTextChanged));



        public static readonly DependencyProperty TextProperty =
        DependencyProperty.RegisterAttached(nameof(ValueText), typeof(string), typeof(LabelTextBox),
            new FrameworkPropertyMetadata(default(string), Text_OnTextChanged));



        public string LabelText
        {
            get => (string)GetValue(LabelProperty);
            set => SetValue(LabelProperty, value);
        }
        public string ValueText
        {
            get => (string)GetValue(TextProperty);
            set => SetValue(TextProperty, value);
        }


        public LabelTextBox()
        {
            InitializeComponent();
        }

        private static void Text_OnTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            LabelTextBox textBox = (LabelTextBox)d;

            textBox.TextBox.Text = (string)e.NewValue;
        }

        private static void Label_OnTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            LabelTextBox textBox = (LabelTextBox)d;

            textBox.Label.Content = e.NewValue;
        }
    }
}
