﻿using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace CustomControls.TextBoxes
{
    public sealed partial class PasswordBoxHint : TextBox
    {
        public static readonly DependencyProperty HintProperty =
            DependencyProperty.RegisterAttached(nameof(Hint), typeof(string), typeof(PasswordBoxHint),
                new FrameworkPropertyMetadata(default(string), FrameworkPropertyMetadataOptions.AffectsRender, Property_OnTextChanged));

        public string Hint
        {
            get => (string)GetValue(HintProperty);
            set => SetValue(HintProperty, value);
        }

        public PasswordBoxHint()
        {
            InitializeComponent();

            TextChanged += TextBoxHint_TextChanged;
        }

        private void TextBoxHint_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(Hint)) return;

            if (Text.Length > 0)
            {
                Background = null;
            }
            else
            {
                Background = DrawWaterMark(Hint, FontSize, FontFamily);

            }
        }

        private static DrawingBrush DrawWaterMark(string textString, double fontSize, FontFamily fontFamily)
        {
            DrawingGroup drawingGroup = new DrawingGroup();

            using DrawingContext drawingContext = drawingGroup.Open();

            FormattedText formattedText = new FormattedText(
                textString,
                CultureInfo.CurrentUICulture,
                FlowDirection.LeftToRight,
                new Typeface(fontFamily.ToString()),
                fontSize,
                Brushes.Black,
                1
            );

            Geometry textGeometry = formattedText.BuildGeometry(new Point(0, 0));

            drawingContext.DrawGeometry(Brushes.Black, new Pen(Brushes.Black, 1), textGeometry);

            DrawingBrush drawing = new DrawingBrush(drawingGroup)
            {
                Stretch = Stretch.None
            };

            return drawing;
        }

        private static void Property_OnTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PasswordBoxHint textBox = (PasswordBoxHint)d;

            textBox.Hint = (string)e.NewValue;

            textBox.Background = DrawWaterMark((string)e.NewValue, textBox.FontSize, textBox.FontFamily);
        }

        private void TextBox_PreviewExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            if (e.Command == ApplicationCommands.Copy ||e.Command == ApplicationCommands.Cut)
            {
                e.Handled = true;
            }
        }
    }
}
