﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CustomControls.TextBoxes
{
    public partial class LabelLabelBox : UserControl
    {
        public static readonly DependencyProperty LabelNameProperty =
DependencyProperty.RegisterAttached(nameof(LabelName), typeof(object), typeof(LabelLabelBox),
new FrameworkPropertyMetadata(default(string), Label_OnTextChanged));


        public static readonly DependencyProperty LabelProperty =
DependencyProperty.RegisterAttached(nameof(LabeValue), typeof(object), typeof(LabelLabelBox),
new FrameworkPropertyMetadata(default(string), LabelValue_OnTextChanged));

   

        public string LabeValue
        {
            get => (string)GetValue(LabelProperty);
            set => SetValue(LabelProperty, value);
        }
        public string LabelName
        {
            get => (string)GetValue(LabelNameProperty);
            set => SetValue(LabelNameProperty, value);
        }

        public LabelLabelBox()
        {
            InitializeComponent();
        }

        private static void Label_OnTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            LabelLabelBox label = (LabelLabelBox)d;

            label.Label.Content = e.NewValue;
        }

        private static void LabelValue_OnTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            LabelLabelBox label = (LabelLabelBox)d;

            label.LabelValue.Content = e.NewValue;
        }
    }
}
