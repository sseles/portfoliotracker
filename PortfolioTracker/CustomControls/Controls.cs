﻿using System;
using System.Windows;

namespace CustomControls
{
    public static class Controls
    {
        public static void Configuration()
        {
            ResourceDictionary();
        }

        private static void ResourceDictionary()
        {
            bool isValidUri = Uri.TryCreate("/CustomControls;component/ControlsDictionary.xaml", UriKind.Relative, out Uri uri);

            if (isValidUri == true)
            {
                ResourceDictionary dictionary = (ResourceDictionary)Application.LoadComponent(uri);

                Application.Current.Resources.MergedDictionaries.Add(dictionary);
            }
        }
    }
}
