﻿using System.Reflection;

namespace CustomControls.Extensions
{
    public static class PropertyCopier
    {
        public static T CopyFrom<T>(this T destination, T source) where T : class
        {
            PropertyInfo[] properties = typeof(T).GetProperties();

            foreach (PropertyInfo property in properties)
            {
                property.SetValue(destination, property.GetValue(source));
            }

            return destination;
        }
    }
}
