﻿using System;

namespace CustomFunctions.Extensions
{
    public static class StringComparer
    {
        public static int ComparisonScore(this string first, string second)
        {
            int n = first.Length;
            int m = second.Length;

            if (n == 0)
            {
                return m;
            }
            else if (m == 0)
            {
                return n;
            }

            int[] p = new int[n + 1]; //'previous' cost array, horizontally
            int[] d = new int[n + 1]; // cost array, horizontally
            int[] _d; //placeholder to assist in swapping p and d

            // indexes into strings s and t
            int i; // iterates through s
            int j; // iterates through t

            char t_j; // jth character of t

            int cost; // cost

            for (i = 0; i <= n; i++)
            {
                p[i] = i;
            }

            for (j = 1; j <= m; j++)
            {
                t_j = second[j - 1];
                d[0] = j;

                for (i = 1; i <= n; i++)
                {
                    cost = first[i - 1] == t_j ? 0 : 1;
                    // minimum of cell to the left+1, to the top+1, diagonally left and up +cost				
                    d[i] = Math.Min(Math.Min(d[i - 1] + 1, p[i] + 1), p[i - 1] + cost);
                }

                // copy current distance counts to 'previous row' distance counts
                _d = p;
                p = d;
                d = _d;
            }
            return p[n];
        }
    }
}
