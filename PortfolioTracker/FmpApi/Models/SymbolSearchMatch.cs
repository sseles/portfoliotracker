﻿using System;
using System.Collections.Generic;
using System.Linq;
using ApiBase.Interfaces.Models;
using CustomFunctions.Extensions;
using Newtonsoft.Json;

namespace FmpApi.Models
{
    public class SymbolSearchResult : ISymbolSearchResult
    {
        [JsonProperty("symbol")]
        public string Symbol { get; set; } = string.Empty;
        [JsonProperty("name")]
        public string Name { get; set; } = string.Empty;
        public string Type { get; set; } = string.Empty;
        [JsonProperty("stockExchange")]
        public string Region { get; set; } = string.Empty;
        [JsonProperty("currency")]
        public string Currency { get; set; } = string.Empty;
        public decimal MatchScore { get; set; }

        public static IEnumerable<SymbolSearchResult> FromJsonList(string json, string symbol)
        {
            List<SymbolSearchResult> result = JsonConvert.DeserializeObject<List<SymbolSearchResult>>(json);

            foreach (SymbolSearchResult symbolSearchResult in result)
            {
                int score;

                if (string.IsNullOrWhiteSpace(symbolSearchResult.Symbol))
                {
                    score = 1000;
                }
                else
                {
                    score = symbolSearchResult.Symbol.ComparisonScore(symbol);
                }

                symbolSearchResult.MatchScore = score;
            }

            return result.OrderBy(p => p.MatchScore);
        }
    }
}
