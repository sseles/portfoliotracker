﻿using System;
using System.Collections.Generic;
using ApiBase.Interfaces.Models;
using Newtonsoft.Json;

namespace FmpApi.Models
{
    public class CompanyOverview : ICompanyOverview
    {
        [JsonProperty("symbol")]
        public string Symbol { get; set; }
        [JsonProperty("companyName")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("exchangeShortName")]
        public string Exchange { get; set; }
        [JsonProperty("currency")]
        public string Currency { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("sector")]
        public string Sector { get; set; }
        [JsonProperty("industry")]
        public string Industry { get; set; }
        public bool HasDividends { get; set; }

        public DateTime? LastUpdate { get; set; }
        public static CompanyOverview FromJson(string json)
        {
            CompanyOverview[] companies =  JsonConvert.DeserializeObject<CompanyOverview[]>(json);

            return companies[0];
        }

        public static IEnumerable<CompanyOverview> ListFromJson(string json)
        {
            return JsonConvert.DeserializeObject<IEnumerable<CompanyOverview>>(json);
        }
    }
}