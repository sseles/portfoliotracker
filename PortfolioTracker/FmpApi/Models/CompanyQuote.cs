﻿using System.Collections;
using System.Collections.Generic;
using ApiBase.Interfaces.Models;
using Newtonsoft.Json;

namespace FmpApi.Models
{
    public class CompanyQuote : ICompanyQuote
    {
        [JsonProperty("symbol")]
        public string Symbol { get; set; }
        [JsonProperty("open")]
        public decimal Open { get; set; }
        [JsonProperty("dayHigh")]
        public decimal High { get; set; }
        [JsonProperty("dayLow")]
        public decimal Low { get; set; }
        [JsonProperty("price")]
        public decimal Price { get; set; }
        [JsonProperty("volume")]
        public uint Volume { get; set; }
        [JsonProperty("previousClose")]
        public decimal PreviousClose { get; set; }
        [JsonProperty("change")]
        public decimal Change { get; set; }
        [JsonProperty("changesPercentage")]
        public string ChangePercent { get; set; }

        public static CompanyQuote FromJson(string json)
        {
            CompanyQuote[] quote = JsonConvert.DeserializeObject<CompanyQuote[]>(json);

            return quote[0];
        }

        public static IEnumerable<CompanyQuote> ListFromJson(string json)  
        {
            return JsonConvert.DeserializeObject<IEnumerable<CompanyQuote>>(json);
        }
    }
}