﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using ApiBase.Interfaces;
using ApiBase.Interfaces.FD;
using FmpApi.FD;
using FmpApi.Properties;

namespace FmpApi
{
    public class FmpService : IStockService
    {
        public ISearchCompaniesService SearchCompaniesService { get; }
        public IQuoteService QuoteService { get; }
        public ICompanyOverviewService CompanyOverviewService { get; }

        private readonly HttpClient _client;

        public FmpService(string key)
        {
            _client = new HttpClient
            {
                BaseAddress = new Uri(Resources.BaseUrl),

                Timeout = TimeSpan.FromSeconds(Convert.ToDouble(10000))
            };

            _client.DefaultRequestHeaders.Accept.Clear();

            _client.DefaultRequestHeaders.Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));

            SearchCompaniesService = new FMPSearchCompanies(_client, key);

            QuoteService = new FMPQuote(_client, key);

            CompanyOverviewService = new FMPCompanyOverview(_client, key);
        }

        public void Dispose()
        {
            _client.Dispose();

            GC.SuppressFinalize(this);
        }
    }
}
