﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using FmpApi.Base;
using FmpApi.Properties;
using ApiBase.Interfaces.FD;
using ApiBase.Interfaces.Models;
using FmpApi.Models;

namespace FmpApi.FD
{
    public class FMPCompanyOverview : ServiceBase, ICompanyOverviewService
    {
        public FMPCompanyOverview(HttpClient client, string key) : base(client, key)
        {
        }

        public async Task<ICompanyOverview> GetCompanyOverview(string symbol, CancellationToken? token)
        {
            string command = string.Format("{0}/{1}?", Resources.Function_Overview, symbol);

            string response = await SendRequest(command);

            return CompanyOverview.FromJson(response);
        }

        public async Task<IEnumerable<ICompanyOverview>> GetCompanyOverviews(params string[] symbols)
        {
            string command = string.Format("{0}/{1}?", Resources.Function_Overview, string.Join(",", symbols));

            string response = await SendRequest(command);

            return CompanyOverview.ListFromJson(response);
        }

        public async Task<IEnumerable<ICompanyOverview>> GetCompanyOverviews(CancellationToken? token, params string[] symbols)
        {
            return await GetCompanyOverviews(symbols);
        }
    }
}
