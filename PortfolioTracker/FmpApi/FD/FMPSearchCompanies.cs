﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using ApiBase.Interfaces.FD;
using ApiBase.Interfaces.Models;
using FmpApi.Base;
using FmpApi.Models;
using FmpApi.Properties;

namespace FmpApi.FD
{
    public class FMPSearchCompanies : ServiceBase, ISearchCompaniesService
    {
        public FMPSearchCompanies(HttpClient client, string key) : base(client, key)
        {

        }

        public async Task<IEnumerable<ISymbolSearchResult>> SearchCompanies(string keywords, CancellationToken? token)
        {
            string command = string.Format("{0}?{1}{2}&", Resources.Function_SymbolSearch, Resources.Query, keywords);

            string response = await SendRequest(command);

            return SymbolSearchResult.FromJsonList(response, keywords);
        }
    }
}
