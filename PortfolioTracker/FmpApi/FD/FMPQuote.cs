﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using FmpApi.Base;
using FmpApi.Properties;
using ApiBase.Interfaces.FD;
using ApiBase.Interfaces.Models;
using FmpApi.Models;

namespace FmpApi.FD
{
    public class FMPQuote : ServiceBase, IQuoteService
    {
        public FMPQuote(HttpClient client, string key) : base(client, key)
        {
        }

        public async Task<ICompanyQuote> GetQuote(string symbol, CancellationToken? token)
        {
            string command = string.Format("{0}/{1}?", Resources.Function_Quote, symbol);

            string response = await SendRequest(command);

            return CompanyQuote.FromJson(response);
        }

        public async Task<IEnumerable<ICompanyQuote>> GetQuotes(params string[] symbols)
        {
            string command = string.Format("{0}/{1}?", Resources.Function_Quote, string.Join(",", symbols));

            string response = await SendRequest(command);

            return CompanyQuote.ListFromJson(response);
        }

        public async Task<IEnumerable<ICompanyQuote>> GetQuotes(CancellationToken? token, params string[] symbols)
        {
            return await GetQuotes(symbols);
        }
    }
}
