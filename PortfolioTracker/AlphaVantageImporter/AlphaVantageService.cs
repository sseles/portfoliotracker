﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using AlphaVantageApi.FD;
using AlphaVantageApi.Properties;
using ApiBase.Interfaces;
using ApiBase.Interfaces.FD;

namespace AlphaVantageApi
{
    public class AlphaVantageService : IStockService
    {
        public ISearchCompaniesService SearchCompaniesService { get; }
        public IQuoteService QuoteService { get; }
        public ICompanyOverviewService CompanyOverviewService { get; }

        private readonly HttpClient _client;

        public AlphaVantageService(string key)
        {
            _client = new HttpClient
            {
                BaseAddress = new Uri(Resources.BaseUrl),

                Timeout = TimeSpan.FromSeconds(Convert.ToDouble(10000))
            };

            _client.DefaultRequestHeaders.Accept.Clear();

            _client.DefaultRequestHeaders.Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));

            SearchCompaniesService = new AVSearchCompanies(_client, key);

            QuoteService = new AVQuote(_client, key);

            CompanyOverviewService = new AVCompanyOverview(_client, key);
        }

        public void Dispose()
        {
            _client.Dispose();

            GC.SuppressFinalize(this);
        }
    }
}
