﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using AlphaVantageApi.Base;
using AlphaVantageApi.Models;
using AlphaVantageApi.Properties;
using ApiBase.Interfaces.FD;
using ApiBase.Interfaces.Models;

namespace AlphaVantageApi.FD
{
    public class AVSearchCompanies : ServiceBase, ISearchCompaniesService
    {
        public AVSearchCompanies(HttpClient client, string key) : base(client, key)
        {

        }

        public async Task<IEnumerable<ISymbolSearchResult>> SearchCompanies(string keywords, CancellationToken? token)
        {
            string command = string.Format("{0}{1}{2}{3}", Resources.BaseFunction, Resources.Function_SymbolSearch, Resources.KeyWords, keywords);

            string response = await SendRequest(command);

            return SymbolSearchResult.FromJsonList(response);
        }
    }
}
