﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using AlphaVantageApi.Base;
using AlphaVantageApi.Models;
using AlphaVantageApi.Properties;
using ApiBase.Interfaces.FD;
using ApiBase.Interfaces.Models;

namespace AlphaVantageApi.FD
{
    public class AVQuote : ServiceBase, IQuoteService
    {
        public AVQuote(HttpClient client, string key) : base(client, key)
        {
        }

        public async Task<ICompanyQuote> GetQuote(string symbol, CancellationToken? token)
        {
            string command = string.Format("{0}{1}{2}{3}", Resources.BaseFunction, Resources.Function_Quote, Resources.Symbol, symbol);

            string response = await SendRequest(command);

            return CompanyQuote.FromJson(response);
        }

        public async Task<IEnumerable<ICompanyQuote>> GetQuotes(params string[] symbols)
        {
            List<ICompanyQuote> list = new List<ICompanyQuote>(symbols.Length);

            foreach (string symbol in symbols)
            {
                list.Add(await GetQuote(symbol, null));
            }

            return list;
        }

        public async Task<IEnumerable<ICompanyQuote>> GetQuotes(CancellationToken? token, params string[] symbols)
        {
            List<ICompanyQuote> list = new List<ICompanyQuote>(symbols.Length);

            foreach (string symbol in symbols)
            {
                list.Add(await GetQuote(symbol, token));
            }

            return list;
        }
    }
}
