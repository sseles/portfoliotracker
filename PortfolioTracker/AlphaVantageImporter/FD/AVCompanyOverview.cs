﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using AlphaVantageApi.Base;
using AlphaVantageApi.Models;
using AlphaVantageApi.Properties;
using ApiBase.Interfaces.FD;
using ApiBase.Interfaces.Models;

namespace AlphaVantageApi.FD
{
    public class AVCompanyOverview : ServiceBase, ICompanyOverviewService
    {
        public AVCompanyOverview(HttpClient client, string key) : base(client, key)
        {
        }

        public async Task<ICompanyOverview> GetCompanyOverview(string symbol, CancellationToken? token)
        {
            string command = string.Format("{0}{1}{2}{3}", Resources.BaseFunction, Resources.Function_Overview, Resources.Symbol, symbol);

            string response = await SendRequest(command);

            return CompanyOverview.FromJson(response);
        }

        public async Task<IEnumerable<ICompanyOverview>> GetCompanyOverviews(params string[] symbols)
        {
            List<ICompanyOverview> list = new List<ICompanyOverview>(symbols.Length);

            foreach (string symbol in symbols)
            {
                list.Add(await GetCompanyOverview(symbol, null));
            }

            return list;
        }

        public async Task<IEnumerable<ICompanyOverview>> GetCompanyOverviews(CancellationToken? token, params string[] symbols)
        {
            List<ICompanyOverview> list = new List<ICompanyOverview>(symbols.Length);

            foreach (string symbol in symbols)
            {
                list.Add(await GetCompanyOverview(symbol, token));
            }

            return list;
        }
    }
}
