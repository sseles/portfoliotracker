﻿using System.Net.Http;
using System.Threading.Tasks;
using AlphaVantageApi.Properties;

namespace AlphaVantageApi.Base
{
    public abstract class ServiceBase
    {
        protected readonly HttpClient _client;

        private readonly string _key;

        protected ServiceBase(HttpClient client, string key)
        {
            _client = client;

            _key = key;
        }

        protected async Task<string> SendRequest(string command)
        {
            HttpResponseMessage response = await _client.GetAsync(CreateUri(command));

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }

            return null;
        }

        private string CreateUri(string command)
        {
            return string.Format("{0}{1}{2}", command, Resources.ApiKey, _key);
        }
    }
}
