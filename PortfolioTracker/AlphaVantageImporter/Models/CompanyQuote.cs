﻿using System;
using ApiBase.Interfaces.Models;
using Newtonsoft.Json;

namespace AlphaVantageApi.Models
{
    public class CompanyQuote : ICompanyQuote
    {
        [JsonProperty("01. symbol")]
        public string Symbol { get; set; }
        [JsonProperty("02. open")]
        public decimal Open { get; set; }
        [JsonProperty("03. high")]
        public decimal High { get; set; }
        [JsonProperty("04. low")]
        public decimal Low { get; set; }
        [JsonProperty("05. price")]
        public decimal Price { get; set; }
        [JsonProperty("06. volume")]
        public uint Volume { get; set; }
        [JsonProperty("07. latest trading day")]
        public DateTime LatestTradingDay { get; set; }
        [JsonProperty("08. previous close")]
        public decimal PreviousClose { get; set; }
        [JsonProperty("09. change")]
        public decimal Change { get; set; }
        [JsonProperty("10. change percent")]
        public string ChangePercent { get; set; }

        public static CompanyQuote FromJson(string json)
        {
            return JsonConvert.DeserializeObject<JsonObject>(json).GlobalQuote;
        }

        private class JsonObject
        {
            [JsonProperty("Global Quote")]
            public CompanyQuote GlobalQuote { get; set; }
        }
    }
}