﻿using System;
using ApiBase.Interfaces.Models;
using Newtonsoft.Json;

namespace AlphaVantageApi.Models
{
    public class CompanyOverview : ICompanyOverview
    {
        [JsonProperty("Symbol")]
        public string Symbol { get; set; }
        [JsonProperty("AssetType")]
        public string AssetType { get; set; }
        [JsonProperty("Name")]
        public string Name { get; set; }
        [JsonProperty("Description")]
        public string Description { get; set; }
        [JsonProperty("Exchange")]
        public string Exchange { get; set; }
        [JsonProperty("Currency")]
        public string Currency { get; set; }
        [JsonProperty("Country")]
        public string Country { get; set; }
        [JsonProperty("Sector")]
        public string Sector { get; set; }
        [JsonProperty("Industry")]
        public string Industry { get; set; }
        [JsonProperty("BookValue")]
        public string BookValue { get; set; }
        [JsonProperty("DividendPerShare")]
        public decimal DividendPerShare { get; set; }
        [JsonProperty("DividendYield")]
        public decimal DividendYield { get; set; }
        [JsonProperty("ExDividendDate")]
        public DateTime ExDividendDate { get; set; }
        [JsonProperty("DividendDate")]
        public DateTime DividendDate { get; set; }
        [JsonProperty("FiscalYearEnd")]
        public string FiscalYearEnd { get; set; }
        [JsonProperty("RevenuePerShareTTM")]
        public string RevenuePerShareTTM { get; set; }
        [JsonProperty("ProfitMargin")]
        public string ProfitMargin { get; set; }
        [JsonProperty("RevenueTTM")]
        public string RevenueTTM { get; set; }
        [JsonProperty("OperatingMarginTTM")]
        public string OperatingMarginTTM { get; set; }
        [JsonProperty("50DayMovingAverage")]
        public string FiftyDayMovingAverage { get; set; }
        [JsonProperty("200DayMovingAverage")]
        public string TwoHundredDayMovingAverage { get; set; }
        public bool HasDividends { get; set; }
        public DateTime? LastUpdate { get; set; }
        public static CompanyOverview FromJson(string json)
        {
            return JsonConvert.DeserializeObject<CompanyOverview>(json);
        }
    }
}