﻿using System;
using System.Collections.Generic;
using System.Linq;
using ApiBase.Interfaces.Models;
using Newtonsoft.Json;

namespace AlphaVantageApi.Models
{
    public class SymbolSearchResult : ISymbolSearchResult
    {
        [JsonProperty("1. symbol")]
        public string Symbol { get; set; } = string.Empty;

        [JsonProperty("2. name")]
        public string Name { get; set; } = string.Empty;

        [JsonProperty("3. type")]
        public string Type { get; set; } = string.Empty;

        [JsonProperty("4. region")]
        public string Region { get; set; } = string.Empty;

        [JsonProperty("5. marketOpen")]
        public TimeSpan MarketOpen { get; set; }

        [JsonProperty("6. marketClose")]
        public TimeSpan MarketClose { get; set; }

        [JsonProperty("7. timezone")]
        public string Timezone { get; set; } = string.Empty;

        [JsonProperty("8. currency")]
        public string Currency { get; set; } = string.Empty;

        [JsonProperty("9. matchScore")]
        public decimal MatchScore { get; set; }

        public static IEnumerable<SymbolSearchResult> FromJsonList(string json)
        {
            JsonObject jsonList = JsonConvert.DeserializeObject<JsonObject>(json);
            
            return jsonList.BestMatches.OrderByDescending(p => p.MatchScore);
        }

        private class JsonObject
        {
            public IEnumerable<SymbolSearchResult> BestMatches { get; set; }
        }
    }
}
